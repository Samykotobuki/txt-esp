Kanagi: ...
Pluma Negra: Resistirse es inutil.
Pluma Negra: No lograrás nada eliminando más [textBlue:Rumors].
Kanagi: ¡Ha! Eso es gracioso. No estoy buscando lograr@nada.
Kanagi: Solo estoy sacándome la frustración.
Pluma Negra: Por favor intenta razonar, Kanagi.
Pluma Negra: Estamos pidiéndote que te unas a nosotras en las Alas@de Magius, como lo hizo Tsukasa.
Kanagi: Eres un desastre.
Kanagi: Estoy honestamente lívida de que siquiera@se te ocurra sugerir semejante cosa.
Kanagi: Especialmente considerando que son ustedes@las que me abandonaron.
Pluma Negra: Así que no vas a venir con nosotras...
Kanagi: Ruega todo lo que quieras. No pasará.
Kanagi: De hecho, creo que te deberian dar una leccion@por tu impudencia.
Kanagi: ¡Espero que estés totalmente lista para@lo que está por pasar!
Kanagi: ¿Oh? Disculpas. Tengo que tomar esta llamada.
