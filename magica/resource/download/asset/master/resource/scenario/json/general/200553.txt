¡Feliz Año Nuevo!
En realidad estaba a punto@de ir a visitar el santuario.
Ya que nos hemos encontrado así...
¿Quizás te gustaría venir conmigo?
¿Qué me dices?
¡Oh! ¡Oh santo cielo, mira!@'Gran Fortuna'
*Risilla*
¡Parece que hacer fila para rezar@a primera hora de la mañana@valió la pena!
... Um.
Tal vez me emocioné demasiado.
Veamos...@¿Debería hacer algo de comer además@de las habituales cajas de Año Nuevo...?
Hmm...
¡Ah! ¡Sopa de Mochi podría estar bien!
¡Ah! ¡Sopa de Mochi podría estar bien!
¡Ah! ¡Sopa de Mochi podría estar bien!
Espero que todos puedan venir hoy.
Cuando el sol empieza a ponerse,@la temperatura baja de golpe, ¿verdad?
Me pregunto cómo pasa Kyoko@sus días cuando es así...
Espero que no se haya resfriado.
*Risilla*
¿Adivina qué? ¡Madoka vino con todas@las demás a divertirse conmigo hoy!
Es realmente maravilloso pasar el@Año Nuevo con compañía, ¿verdad?
Es tan animado y cálido.
Tengo pensado pasar este Año Nuevo@relajándome de sol a sol.
Así que he terminado todos mis tareas@de invierno con antelación.
... No. Me temo que no puedo@dejar que las veas.
¡Deberías hacer un esfuerzo@por tu cuenta, primero!
Nada me gustaría más que sentarme y@disfrutar del ambiente de Año Nuevo...
Sin embargo,@siento la presencia de una Bruja.
El año acaba de empezar,@¡pero debemos darnos prisa y encontrarla!
Somos Chicas Mágicas, después de todo.
¡No! ¡Soy yo!
¡Soy la real, lo juro!
*Suspiro*
Cielos, esa horrible Bruja de los Espejos.@¡Ya está haciendo copias de nosotras@en nuestras mejores galas...!
Yo... no puedo decir estar complacida.
Especialmente si lleva a@malentendidos como este...
El Año Nuevo requiere sopa de@frijoles rojos con mochi horneado,@¿no crees?
¡Ah, y quizá también algunos@postres tradicionales!
¡Quedarán muy bien juntos!
Recibí una tarjeta de Año Nuevo...
Pero parece que alguien la dejó@directamente en mi buzón@mientras no miraba.
Hablando de eso,@Sayaka dijo que a ella le pasó lo mismo...
No crees que sean de Kyoko, ¿o sí?
Toma. Estos son algunos paquetes@de calor desechables.
Siéntete libre de usarlos, ¿de acuerdo?
Hace bastante frío hoy.
No es bueno dejar que tu estómago@se enfríe, ¿sabes?
¡Ooh, un puesto de tiro al blanco!
Apuesto a que puedo darle en el blanco@todas las veces.
¡Dime si ves algo que te guste!
Le dispararé hasta al último por ti...
*Risilla*
¡Oh, lo siento mucho!
Nos serví té negro por costumbre.
Quería hacer té verde...
Espero que no desentone demasiado,@sobre todo porque estamos todos@bien vestidos.
Ponerte tus mejores prendas influye@mucho en tu estado de ánimo, ¿no crees?
Desde que me vestí con mi kimono hoy...
¡Me he sentido muy emocionada!
Ha sido increíblemente divertido.
Madoka, Homura, Sayaka, Kyoko...
Todas las demás en Kamihama, también.@He conocido a tanta gente.
Así que este año, no pedí un deseo@al dios del santuario.
¡Le di las gracias!
Cuando veo familias caminando juntas@por la ciudad, o en los santuarios...
Me recuerda a cómo solía@tener eso también.
Supongo que todavía soy...
... una niña...
¡En realidad encontré este kimono@en el almacén!
Me estaba adelantando a la limpieza@de primavera.
No esperaba que me quedara perfecto,
... así que estaba...
... un poco sorprendida...
