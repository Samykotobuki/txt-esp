Tsuruno: ¡Ah! Ahí está.
Felicia: ¡Tsuruno!
Tsuruno: Whoooa... ¡Buen trabajo! ¡Trajiste a ´muchas Chicas Mágicas!
Tsuruno: Espera, ¿Por qué tienes los ojos rojos?
Felicia: ¡Dijeron que si no podía explicar@todo, me venderían!
Akira: ¿De dónde demonios ha salido eso?
Tsuruno: [chara:100301:effect_emotion_angry_1][se:7218_anger]¡¿Qué?!@¡Cómo te atreves a decirle eso a mi amiga!
Hazuki: No hemos dicho eso en absoluto.
Tsuruno: No lo pensé.
Nanaka: ¿Podemos evitar esas ridículas acusaciones?@Ahora no es el momento.
Nanaka: Sólo estábamos interrogando a esta chica sobre los@detalles de la situación actual.
Tsuruno: ¿Así que su explicación no tenía sentido?
Nanaka: Precisamente.
Tsuruno: ¡Bien, entonces!
Tsuruno: ¡Deja que la Chica Mágica más Poderosa te dé@la explicación más Poderosa!
Nanaka: ¿La explicación... más poderosa?
Felicia: [chara:100501:effect_emotion_surprise_0][se:7226_shock]¡Ajá!
Felicia: ¡Tsuruno, no lo hagas!
Tsuruno: ¡¿Qué?! ¿Por qué no?
Felicia: ¡Si sigues tomando toda la carga, te convertirás en un Uwasa otra vez!
Felicia: ¡Tengo que ser yo quien lo explique!
Tsuruno: Lo estás pensando demasiado.
Tsuruno: Esto es solo un caso en el que nos cubrimos@mutuamente las debilidades.
Nanaka: Entonces, ¿podemos pedirte que nos expliques?
Tsuruno: ¡Claro que sí! Deja que me encargue yo.
Nanaka: Ya veo...
Hazuki: Así que una forma de liberarnos de los peligros@de la lucha contra las brujas.
Hazuki: El problema son sus métodos...
Tsuruno: Sí. No entiendo muy bien cómo se supone que@funciona su idea de liberarnos...
Tsuruno: Pero, no hace mucho, intentaron sacrificar a los@ciudadanos de Kamihama.
Tsuruno: También me utilizaron a mí para hacerlo.
Akira: Espera, todavía estoy tratando de entender lo@enorme que es esto...
Nanaka: Sacrificarían una ciudad para asegurar el@futuro de miles de millones de vidas.
Nanaka: Puede que logren sus objetivos de esa manera,@pero no puedo aceptar sus métodos.
Akira: También intentaban ofrecernos a Kako y a mí@como sacrificios.
Tsuruno: Sí, a eso me refiero.
Tsuruno: Todo lo que te acabo de contar es solo una@pequeña parte de lo que está pasando.
Tsuruno: Puedes escuchar el resto donde la Coordinadora.
Tsuruno: Vamos a hacer que todos se reúnan allí para@poder explicarlo.
Hazuki: El local de la Coordinadora es un buen lugar@para entrar en más detalles.
*Ring* *Ring*
Emiri: ¡Whoa! ¡Es una completa casa de locos ahí fuera!
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡Ah! ¡Emily-sensei!
Emiri: ¡Ah, hey Tsurupi!
Emiri: Por supuesto que estás aquí. Te irías corriendo a@una cafetería en cuanto las cosas se pusieran feas.
Tsuruno: ¡En realidad, iba a ir a la sala de consejo justo@después de esto!
Emiri: ¡¿Estás en serio?!
Emiri: ¡Me alegro de haberte pillado antes de que te@fueras! Hablando de suerte, ¿tengo razón?
Meiyui: Debo recalcar que correr a este restaurante fue@idea mía.
Tsuruno: ¡Whoa! ¡Meiyui-sensei también está aquí!
Meiyui: Ha pasado mucho tiempo. ¿Estás bien?
Tsuruno: ¡Claro que sí!
*Ring* *Ring*
Konoha: ¡Ayame, no! ¡No debes correr dentro del@restaurante!
Ayame: ¡Pero tengo que encontrar a Hazuki!
Felicia: ¡Ah! ¡Ayame!
Ayame: ¡Conozco esa voz! ¡Es Felicia!
Ayame: ¡Espera, ya sé por qué estás aquí!
Ayame: Estabas huyendo de esas chicas raras de las@túnicas, ¿verdad?
Felicia: [chara:100501:effect_emotion_angry_0][se:7219_strain]¿Huyendo?
Felicia: [chara:100501:effect_emotion_angry_1][se:7218_anger]¿Qué? ¡Claro que no!@¡No podrías estar más equivocada!
Felicia: ¡Estaba siendo una heroína! ¡Estaba ahí fuera@salvando a todo el mundo!
Konoha: ¡Ayame! Estamos en un restaurante. Baja la voz.
Ayame: Pero Felicia fue la que...
Konoha: Te compraré un tazón de helado si te@quedas callada.
Ayame: ¡¿De verdad?!
Felicia: ¡Tsuruno! ¡Yo también quiero un tazón de helado!
Tsuruno: ¡No te pongas a competir por una tazón de helado!
Akira: Se está volviendo difícil controlarlas..
Nanaka: Es solo cuestión de tiempo que nos pidan que@abandonemos el local.
Nanaka: Deberíamos irnos antes de que nos prohíban la@entrada a este restaurante para siempre.
Nanaka: De acuerdo. Nos adelantaremos a la tienda de la@coordinadora.
Nanaka: Nos vemos luego.
Tsuruno: ¡Sí, hasta luego!
Ayame: ¡No dejes que te atrapen, Felicia!
Felicia: [chara:100501:effect_emotion_angry_1][se:7218_anger]¡Como si pudieran atraparme! ¡Tonta!
Ayame: [chara:302801:effect_emotion_angry_1][se:7218_anger]¡¿A quién llamas estúpido tonta, tonta?!
Konoha: Vamos, Ayame, es hora de irse.
Tsuruno: Sí que estaba animado ahí atrás.
Felicia: ¿Cómo es que no vamos también a la casa de@la Coordinadora?
Tsuruno: Acabo de percibir algo... Creo que es la magia@de otra Chica Mágica.
Tsuruno: Lo sabía, ella sigue ahí...
Felicia: ¿Eh? Espera, esa es...
Tsuruno: ¿Sabes quién es?
Felicia: ¿Shizuku? ¿Verdad?
Tsuruno: ¡Ah! ¡Sí, quizá lo sea!
Tsuruno: Vamos a comprobarlo de nuevo...
Tsuruno: ¿Eh? Ya no puedo sentir su magia.
