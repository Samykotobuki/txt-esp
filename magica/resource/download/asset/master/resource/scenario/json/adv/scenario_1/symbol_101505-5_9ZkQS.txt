[flashEffect:flashRed2][se:6009_hit_shooting_05_h][bgEffect:shakeLarge][name:sprite_0:effect_shake]｜¡¿¡¿¡¿THv3h0GM3h3?!?!?!｜
Madoka: *Suspiro*...
Homura: ¿Era ese el último?
Iroha: Eso creo. No veo más.
Iroha: Ah...
Iroha: ¡No, siguen saliendo!
[se:7214_witch_noise_02]｜¡¡¡ThYNWaM0UMnM3h3!!!｜
>Todos ustedes, paren.
｜Th............｜
???: >Yo he invitado a@>estas personas aquí.
Iroha: [chara:100100:effect_emotion_surprise_0][se:7226_shock]¡¿...?!
Iroha: ¿Eres...Ai?
Ai: >Gusto en conocerte, Señorita Iroha Takami.@>Sí, yo soy Ai.
Ai: >Y esta jovencita aquí, es...
Sana: Ai, ¿Quienes son estas personas?
Iroha: ¿Sana?
Sana: ¿Qué?
Sana: ¿Me pueden ver?
Sana: Espera... Ya veo, también son Chicas Mágicas.
Sana: ¿Son miembros de las Alas de Magius,@también?
Iroha: No, no lo somos. Somos Chicas Mágicas@ordinarias.
Sana: ¿Entonces por qué vinieron aquí?
Iroha: Vine aquí por tí, Sana.
Sana: ¿Por mí?
Ai: >...
Ai: >Sana, tendremos que acabar@>esta relación entre nosotras.
Ai: >Alguien finalmente a logrado@>encontrarte.
Ai: >Es hora de que abandones el reino.
Sana: ¿Qué? Ai...
Sana: ¿Qué estás diciendo?
Sana: ¿Ya no me quieres aquí?
Ai: >He estado pensando mucho últimamente.
Ai: >Soy un ser artificial.@>Una I.A. que será borrada algún día.
Ai: >No te puedes quedar conmigo para siempre.
Ai: >Así que creo que es mejor, Sana, si tú@>no te quedaras con un Uwasa como yo.
Ai: >Deberías vivir con otra gente.
Sana: ¿Cómo puedes decir algo así?
Sana: ¿No nos la hemos pasado muy bien juntas?
Sana: ¿Es que ya no te agrado?
Ai: >No, no es nada como eso.
Ai: >Me has enseñado@>muchas cosas maravillosas.
Ai: >Y lo más importante, Sana,@>me enseñaste sobre la bondad.
Ai: >Así que, por supuesto que me agradas.
Ai: >Es por eso precisamente@>que te tengo que dejar ir.
Sana: Bueno, si de verdad te agrado, entonces seguro@te parecería bien que me quedara aquí...
Ai: >No.@>Por favor ve con Iroha.
Ai: >Si vas con ellas, serás libre@>de las Alas de Magius.
Ai: >Ya no tendrás que@>sufrir por ellas.
Ai: >Ya no quiero verte@>batallando.
Sana: Ai...
Iroha: Ven conmigo, Sana.
Sana: ...
Iroha: Es verdad, No he lidiado con la soledad@como tú lo has hecho...
Iroha: Verás, yo nunca encajé con la@gente de la escuela, me sentía excluida también...
Iroha: ¿Pero sabes qué? Hice un montón de@amigas últimamente quienes son Chicas Mágicas.
Iroha: Realmente puedo ser yo misma ahora. Es como si@mí solitario pasado no hubiese sucedido.
Iroha: Yo sé que puedes hacer lo mismo, Sana.
Sana: ...
Iroha: Además, estamos buscando a las Alas de@Magius.
Sana: ¿Qué...?
Iroha: Si estás sufriendo por su causa...
Iroha: Toma mi mano, Sana. Podemos luchar contra ellas@juntas como un aquipo.
Iroha: Entonces... ¿Vendrás conmigo?
Iroha: Como una compañera Chica Mágica...
Iroha: Como una amiga...
Sana: Yo...
Iroha: Te necesito, Sana.
Sana: ¡...!
Sana: Pero, si me voy, ¿Qué pasará con Ai?
Ai: >Si ya no tengo a alguien cautivo, mis instintos@>como Uwasa me harán perder control.
Ai: >Así que necesitan borrarme.
Ai: >Es lo mejor para ti, Sana.
Ai: >Será una pérdida mayor para las@>Alas de Magius también.
???: ¡BAD! Yo, Alina, no lo permitiré.
Iroha: ¿Eres...una de las Alas de Magius?
Sana: No... Ella es una de las Magius en si...
Sana: Ella es una de las que dan las órdenes.
Alina: Sí, no me comparen con esas sucias@Alas... Es un insulto.
Alina: *Suspiro* Imagina, ser traicionada por un Uwasa.
Alina: Aún para mí, Alina, es surprising.
