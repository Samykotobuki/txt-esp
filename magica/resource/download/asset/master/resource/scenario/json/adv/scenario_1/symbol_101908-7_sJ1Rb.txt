Kanagi: Phew.
Kanagi: Lograron conseguir unas muy buenas copias@de sí mismas.
Yachiyo: Aunque eran notablemente más débiles que las reales.
Yachiyo: Dudo que esas fueran las únicas copias.
Kanagi: Tendremos que tener cuidado desde ahora.
Yachiyo: Si. Que movimiento tan encubierto.
Pluma Negra: ...
Tsuruno: Iroha...
Iroha: Les pregunté a esas chicas sobre su@situación antes de recuperar mi Soul Gem.
Iroha: Me siento tan mal por ellas. Recuerdan ser@traicionadas por las Magius.
Iroha: Y lo recuerdan muy bien.
Tsuruno: Si, pero no podemos cumplir sus deseos.
Iroha: Si, sé eso.
Iroha: Apurémonos.
Iroha: Aún no hemos encontrado donde se esconden@las Magius reales.
