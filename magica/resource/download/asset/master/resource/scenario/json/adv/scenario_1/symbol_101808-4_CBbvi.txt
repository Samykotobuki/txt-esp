Hinano: Eran mucho más fuertes de lo que esperaba,@pero conseguimos acabar con ellas.
Tsuruno: Sí, habría sido difícil luchar sola...
Tsuruno: ¡Ah, sí, aquí tienes!
Hinano: Espera, aguanta...
Hinano: ¿Estás segura de que puedo tomar esta Grief Seed?
Tsuruno: Sí, quiero decir, quién sabe a qué nos@enfrentaremos después.
Hinano: Es cierto, pero ¿Tienes acaso tienes más?
Tsuruno: Uh, bueno, um...
Hinano: Entonces deberías tomarla.
Hinano: Tu sentido de la justicia es admirable, pero@tienes que cuidarte.
Hinano: Quédate con ella. De todos modos, tengo una.
Tsuruno: ¡Está bien, si tú lo dices! ¡Gracias!
Hinano: Ayudar es una calle de doble sentido.
Hinano: De todos modos, ¿puedes creer lo qué hicieron?
Hinano: ¿En qué estaban pensando al dejar a una bruja@aquí fuera?
Tsuruno: No solo eso, sino que además están CRIANDO brujas.
Hinano: [chara:300301:effect_emotion_surprise_0][se:7226_shock]¿Qué? ¡Esto es una absoluta locura!
Hinano: ¡¿Qué están tratando de hacer?!
Tsuruno: (Supongo que no hay forma de evitar@explicar esto...)
Hinano: Ahora lo entiendo. La aniquilación, la@Coordinadora, todo eso...
Tsuruno: Sí, por eso tenemos que dirigirnos a casa de@la Coordinadora tan rápido como podamos.
Hinano: No, ahora que sé lo que está pasando, no puedo ir@directamente allí.
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡¿Quéeeee?! ¡¿Por qué no?!
Hinano: Estoy preocupada por mis asistentes.
Hinano: Voy a tomar un desvío.
Tsuruno: ¡Aww, mírate, eres tan responsable a pesar de@ser tan pequeña!
Hinano: ¡No me llames pequeña!
Hinano: Espera, ¿Qué edad crees que tengo?
Tsuruno: Umm... Probablemente seas...
Hinano: ¿Sí?
Tsuruno: ...mucho más joven que yo?
Hinano: [chara:300301:effect_emotion_angry_1][se:7218_anger]¡Soy mayor que tú!
Tsuruno: ¡¿Quéeeee?!
Hinano: ¡Soy una veterana, solo superada por@Yachiyo Nanami!
Tsuruno: Vaya. Los genes hacen locuras, ¿verdad?
Hinano: ¡Deja mis genes fuera de esto!
Hinano: Dios...
Hinano: De todos modos, eso es lo que haré.
Hinano: Si veo a alguien por el camino, lo llevaré a@la Coordinadora conmigo.
Hinano: Aunque primero tengo que encontrar a Emiri...
Tsuruno: ¿Emily?
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡¿Emily-sensei?!
Hinano: [chara:300301:effect_emotion_surprise_0][se:7226_shock]¡¿Qué?! ¿Esa idiota, una sensei?
Hinano: Debes estar pensando en otra persona...
Tsuruno: No. Emily-sensei me enseñó mucho.
Tsuruno: En aquel entonces, ella hizo que todas mis@preocupaciones se desvanecieran.
Tsuruno: ¡En estos días, las Chicas Mágicas pasan@constantemente por su sala de orientación!
Hinano: Ah, claro... Eso...
Hinano: Parece que le va mejor de lo que pensaba...
Hinano: De todos modos, voy a ver cómo está esa, eh,@sensei.
Tsuruno: Bien, voy a llamar a Yachiyo.
Tsuruno: ...[wait:1.0][chara:100301:cheek_0][chara:100301:face_mtn_ex_040.exp.json][chara:100301:lipSynch_1][chara:100301:voiceFull_fullvoice/section_101826/vo_full_101808-4-52]¿Eh?
Hinano: ¿Qué pasa?
Tsuruno: No hay servicio.
Hinano: No puede ser— Ah sí, tienes razón.
Hinano: Realmente ha sido un día de locos... ¿Cómo se@supone que vamos a ponernos en contacto?
Tsuruno: ...
Tsuruno: (Yachiyo me dijo que me reuniera con Felicia...)
Tsuruno: (Pero ella nunca contestó su teléfono...)
Tsuruno: (Tal vez debería ir a verla...)
Tsuruno: ¡Está bien, profesora!
Hinano: ¿Eh? ¿Profesora?
Tsuruno: ¡Porque eres la mentora de Emily!
Hinano: Tiene sentido... ¡Espera, no lo tiene!
Hinano: ¿Y? ¿Qué ibas a decir?
Hinano: No creo que  sepas arreglar una red celular,@¿verdad?
Tsuruno: Mi amiga debe estar cerca de la Sala de@Asesoramiento.
Tsuruno: ¡Haré que Emily venga con nosotros!
Hinano: Ya veo.
Hinano: En ese caso, dejaré que te encargues de ella.@Yo iré a buscar a Rika...
Hinano: Realmente es una mierda que nuestros teléfonos@no funcionen.
Tsuruno: ¡Está bien, déjame a Emily-sensei a mí!
Hinano: De acuerdo. ¡Cuento contigo!
Tsuruno: ¡Tengo que encontrar a Felicia!
