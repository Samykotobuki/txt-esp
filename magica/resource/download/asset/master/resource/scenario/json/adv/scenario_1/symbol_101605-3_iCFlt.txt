Momoko: Nuestro grupo no se separó inmediatamente después@de que Mel se convirtiera en bruja.
Momoko: Intentamos durante un tiempo permanecer juntas@y luchar como un equipo...
Momoko: Pero Mifuyu empezó a actuar de forma muy extraña@después de que ocurriera. Era obvio para todas.
Momoko: También no volvimos a ver sonreír a Yachiyo.
Rena: ¿Porque vio a Mel convertirse en bruja?
Momoko: No lo sé... No puedo decirlo con seguridad.
Momoko: Pero desde entonces, Yachiyo empezó a trabajar@por su cuenta más a menudo.
Momoko: Incluso empezó a interferir en la caza de brujas@de otras Chicas Mágicas...
Momoko: Fue entonces cuando empecé a enfadarme con ella.
Momoko: Era como si mi modelo de conducta se rompiera@delante de mí...
Rena: Ella no estaba robando las brujas objetivo@de las otras chicas, ¿verdad?
Momoko: Ahora lo sé.
Momoko: Casi echó a Iroha de la ciudad.
Momoko: Entonces descubrí que estaba investigando@[textBlue:Rumores].
Momoko: Y después de toparme con ella un montón de veces,@entendí lo que intentaba hacer.
Momoko: Estaba haciendo el papel de la mala para intentar@salvar a Iroha y a sus amigas.
Momoko: Pero eso no lo sabía entonces.
Rena: ¿Por eso te distanciaste de ella?
Momoko: No, no del todo...
