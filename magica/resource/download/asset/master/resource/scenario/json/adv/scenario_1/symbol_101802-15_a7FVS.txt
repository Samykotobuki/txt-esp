Yachiyo: No creo que seamos capaces de conectar con el corazón de Mami.
Kanagi: No parece que vayamos a ser capaces de despegarla del Uwasa.
Yachiyo: No. No si no podemos entenderla...
Yachiyo: Estamos completamente rodeadas. No hay forma de salir...
Mami: Gemelas Amane, por favor contengan a estas dos.
Tsukuyo: Grrrgh...
Tsukasa: Nnnghhh...
Mami: Tus gemas del alma serán un buen alimento para Eve.
Mami: Serás parte de la liberación de todas las Chicas Mágicas.
Mami: Deberías alegrarte.
Yachiyo: Kanagi... Deberíamos correr...
Kanagi: ¿Crees que somos capaces de eso, con nuestra magia tan agotada?
Yachiyo: ¡Si trabajamos juntas, al menos deberíamos ser capaces de atravesar las Plumas!
Kanagi: Es cierto... Es demasiado pronto para rendirse todavía.
Mami: [flashEffect:flashWhiteSlow2][se:7211_souljem_02]Ahora, terminemos esto.
Tsuruno: ¡¡¡Maestra!!!
Felicia: ¡¡Yachiyo!!
Mami: A la gente de la Villa Mikazuki le encanta meterse en medio, ¿verdad?
Tsuruno: ¡Hora de nuestro ataque combinado, Felicia! ¡Lánzame hacia ellas con toda la fuerza que puedas!
Felicia: ¡Lo tengo! ¡Aquí vamooos!
Felicia: [flashEffect:flashWhite2][surround:0001_jump01][bgEffect:shakeLarge]¡Hwaaah!
Tsuruno: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:100301_magia]¡Toma ESTO! ¡Flame Waltz!
Pluma Negra: [flashEffect:flashRed2][se:8002_flame_01_h][bgEffect:shakeLarge][chara:715000:effect_shake]¡Gah!
Pluma Blanca: [flashEffect:flashRed2][bgEffect:shakeLarge][chara:715100:effect_shake]¡Guh!
Tsuruno: ¡Podemos pasar, Felicia! ¡Apúrate!
Felicia: ¡Ya voy!
Kanagi: ¡Ustedes dos!
Tsuruno: ¡Toma algunas Grief Seeds!
Yachiyo: ¡Gracias Tsuruno, Felicia!
Yachiyo: ¡Kanagi!
Kanagi: ¡Sí!
Yachiyo: ¿Pero cómo nos encontraste?
Tsuruno: *Risita*
Tsuruno: ¡Una de las chicas que llegó a la tienda de Mitama nos dijo!
Tsuruno: ¡Que las había visto a ti y a Kanagi dirigiéndose al helipuerto!
Felicia: Y cuando vinimos a ver. ¡Aquí estaban!
Yachiyo: Alguien te dijo...
Ayaka: Eh, ¿Van a estar bien solas?
Yachiyo: No te preocupes.
Yachiyo: Solo voy a preguntarles sobre lo que pasó.
Yachiyo: Estábamos confiadas pensando que éramos superheroínas.
Kanagi: Pero este acontecimiento significa que ahora no tenemos que huir.
Mami: Justo cuando estaba a punto de salvarlas...
Mami: Muy bien entonces, no me importa hacerlo con las cuatro.
Mami: ¡Las guiaré a todas!
Pluma Negra: Ungh...gh... Esas locas... ¿Cómo se atreven?
Pluma Negra: E-Espera, ¿Eh?
Yachiyo: ¿Hm?
Pluma Negra: ¡Eeek! ¡¿Yachiyo Nanami?!
Yachiyo: ¡¿...?!
Yachiyo: ¿Q-qué demonios?
Pluma Negra: ¿Qu-qué? ¡No está aquí! ¿Dónde ha ido mi colgante?
Mami: Vuelve conmigo enseguida.
Mami: Debo bautizarte una vez más...
Mami: ¡Como una de las fieles que nos ayudará a@realizar nuestra libertad!
Pluma Negra: ¡Ah, c...c-cierto...!
Yachiyo: ¿Qué? ¡No dejaré que te salgas con la tuya!
Yachiyo: ¡Kanagi! ¡Lee la mente de esa Pluma Negra!
Yachiyo: ¡Está libre de la influencia de los Uwasa, así que puede que sepa algo!
Kanagi: ¡Pero Yachiyo! ¿Quién va a mantener a raya a las demás?
Yachiyo: ¡Ahora somos tres, podemos encargarnos de esto!
Tsuruno: ¡Sí, déjanoslo a nosotras!
Felicia: ¡Los acabaré a TODAS!
Kanagi: ¡Entendido!
Kanagi: Entonces, supongo que esto significa que@miraré dentro de tu mente. 
Kanagi: Hurgaré muy, muy dentro de tu interior.
Pluma Negra: Ah...aaaughhh...
