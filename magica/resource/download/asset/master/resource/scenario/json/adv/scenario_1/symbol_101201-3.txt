Iroha: Eres... Yachiyo, ¿verdad?
Yachiyo: Ah, te acuerdas de mí.
Iroha: Por supuesto que me acuerdo...@Después de lo que intentaste hacerme...
Yachiyo: Supongo que es comprensible.
Iroha: ...
Yachiyo: No tienes que estar a la defensiva.@No quiero hacerte daño.
Yachiyo: Solo pensé en darte una advertencia.
Iroha: ¿Una advertencia...?
Yachiyo: Si…
Yachiyo: No deberías decir eso en Kamihama.
Iroha: ¿Huh?
Yachiyo: Especialmente cuando estás@discutiendo con un amigo.
Yachiyo: Quedarás atrapada por [textRed:el Rumor de@La regla de la ruptura de amistades]...
Iroha: ¿Atrapada por... el rumor?
Yachiyo: ¿No has oído la historia?
¿Ya lo escuchaste? ¿Quién te lo dijo?@[textRed:La regla de la ruptura de amistades].
Si no lo sabes, te arrepentirás.@Si no lo sabes, pasarán cosas malas.
¡Si te retractas, te llamarán mentiroso!
Y entonces este monstruo aterrador te secuestra...@¡Tendrás que limpiar las escaleras para siempre!
Según el rumor que circula entre los@chicos de Kamihama, si te peleas con un amigo,
uno de ustedes desaparecerá... ¡Qué miedo!
Yachiyo: Es más o menos así.
Iroha: Um, entonces... ¿Es esa@[textRed:La regla de la ruptura de amistades]?
Yachiyo: Así es.
Yachiyo: Una vez que se habla de [textRed:terminar una amistad]...
Yachiyo: Pase lo que pase, no te disculpes.
Yachiyo: Si te disculpas y tratas de hacer las paces,@un monstruo vendrá a buscarte.
Iroha: Eso suena muy ridículo...
Yachiyo: Deberías creerlo.
Yachiyo: Es uno de los [textBlue:Rumores] más creíbles@en los archivos de rumores de Kamihama.
Iroha: ¿Kamihama...Qué...?
Yachiyo: ¡Archivos de rumores!
Yachiyo: Los informes de todos los [textBlue:Rumores]extraños@que están invadiendo Kamihama que he recopilado.
Iroha: Ya veo...
Yachiyo: Últimamente, muchos rumores se están@haciendo realidad aquí.
Yachiyo: Incluso hay personas desaparecidas@por algunos de estos [textBlue:Rumores].
Iroha: Eso es—
Yachiyo: No me lo estoy inventando.
Yachiyo: Son tan inusuales como tu Kyubey.
Iroha: *Sorpresa*[chara:100102:effect_emotion_surprise_0][se:7226_shock]
