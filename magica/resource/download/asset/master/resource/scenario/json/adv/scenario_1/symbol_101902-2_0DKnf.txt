Pluma Negra: Urgh.
Sana: ¡M-muy bien! ¡Te atrapé!
Sana: ¡Por favor dime dónde está Iroha!
Pluma Negra: No tengo nada que decirte. Te estás@poniendo en el camino de la liberación.
Sana: ¿Así que todavía confias en Magius?
Sana: ¿Auque las empujaron a todas ustedes Plumas@a un [textBlue:Rumor]?
Pluma Negra: No puedo negar que eso sucedió.
Pluma Negra: Pero me parece inútil tratar@hacer cambiar de parecer a aquellas que siguen en la base.
Pluma Negra: Todas nosotras vamos a trabajar por la liberación, aunque@las Magius nos estén usando.
Pluma Negra: Eso solo es prueba de lo dedicadas que estamos@en conseguir la liberación.
Sana: Pero...
Pluma Negra: Estoy peleando por nuestro futuro. Ese es mi@único deseo.
Sana: ¡...!
Sana: [flashEffect:flashWhite2][surround:1004A01][bgEffect:shakeLarge]E-entonces, ¡lo siento!
Pluma Negra: [flashEffect:flashRed2][surround:1004A06][bgEffect:shakeSmall][chara:715005:effect_shake]Ugh...
Sana: Supongo que no tengo más opción. Tengo que buscar@a esas chicas de hace rato.
