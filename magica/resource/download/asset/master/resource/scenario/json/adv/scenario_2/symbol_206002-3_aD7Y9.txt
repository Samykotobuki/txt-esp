Kyoko: Ahí estás.
Kyoko: Yo me encargo de la Bruja.@Ustedes hagan lo que quieran.
Sayaka: ¡Oye, espera!
Sayaka: Cielos... Hace lo que le da la gana,@como siempre...
Sayaka: Yo también iré, Mami.
Mami: Gracias.
Mami: Homura, ¿puedes ayudar a los cautivos?
Homura: ¡Estoy en ello!
Mami: Madoka, tú y yo brindaremos apoyo.
Madoka: ¡De acuerdo! ¡Nuestras armas de largo alcance@son perfectas para eso!
Sayaka: Quiero ir a ayudar a Kyoko,@pero sólo se interponen en mi camino...
Sayaka: [flashEffect:flashWhite2][surround:2004A09]¡Piérdanse!
Sayaka: ¡No he terminado!
Homura: (No puedo llegar a los cautivos a menos que@derrote a todos estos Familiares.)
Homura: (No quiero hacer las cosas más difíciles para@Madoka, así que...)
Homura: (Necesito deshacerme de ellos rápidamente.)
Homura: ... ¡Fuera de mi camino!
Homura: [flashEffect:flashWhite2][se:4005_equip_arms_grenade_vh]¡Hah!
[flashEffect:flashWhite2][bgEffect:shakeSmall]...
Homura: ¡¿...?!
Homura: ¡Madoka! Gracias.
Homura: (¡Ahora hay una apertura!)
Homura: (Es mi trabajo mantener a los Familiares@alejados de esta gente.)
Homura: (Tengo que protegerlos para que Madoka y@las demás puedan concentrarse en la Bruja.)
Homura: ¡Oh no! ¡Kyoko!
Kyoko: [flashEffect:flashRed2][se:3001_chara_damage][chara:200600:effect_shake][bgEffect:shakeLarge]¡Tgh!
欲土衆音　生聞生彼　已土樂者
Kyoko: Oh, maldición, no se da por vencida.@Va a ser difícil seguir evitando estos ataques.
Kyoko: ¡¿...?!
Kyoko: ¿La cinta de Mami? Me ha alejado.
Mami: Eso estuvo cerca.
Mami: ¿Puedes seguir?
Kyoko: ... ¡Por supuesto que sí!
Kyoko: ¡Espera, mantén esto sobre mí y lánzame sobre@su cabeza!
Kyoko: [flashEffect:flashWhite2][surround:2006A04]¡Listas o no! ¡Aaaaaah!
[flashEffect:flashRed2][surround:2006A03][chara:6107:effect_shake][bgEffect:shakeLarge]願遣目皆　阿德連是　菩薩是妙
Sayaka: [flashEffect:flashWhite2][surround:2004A09]¡Estoy justo detrás de ti! ¡Yaaah!
[flashEffect:flashRed2][surround:2004A13][chara:6107:effect_shake][bgEffect:shakeLarge]言實苦聖　何年終人　六勤白大
Sayaka: Ugh... ¡No quiere caer!
Kyoko: ¡Parece que tenemos una testaruda!
況有樂切　時是世尊　極界世一
Kyoko: ¡Está a punto de atacar de nuevo!@¡Prepárense!
Sayaka: ¡Lo sé!
Sayaka: ... ¡Espera! ¡¿Fue una finta?!
Sayaka: ¡Madoka! ¡Mami!
Kyoko: ¡Tch! ¡Despierten, chicas! ¡Graaahhh!
Sayaka: ¡¿...?!
Sayaka: ... ¡Ya sé!
[flashEffect:flashWhite2][bgEffect:shakeSmall]世汝衆闍　名等世阿　次生處時
Mami y Madoka: ¡Waaah!
Homura: ¡Madoka! ¡Mami!
Homura: (¡Si son golpeadas por eso...!)
Homura: (¡Después de haber vencido a Walpurgisnacht!@¡¿Cómo puede estar pasando esto?!)
Voz de Madoka: ¡No te preocupes, Homura!
Voz de Madoka: ¡Mantén a esa gente a salvo!
Homura: ¡Ah!
Mami: Kyoko y Sayaka nos salvaron cuando@la hicieron perder el equilibrio.
Mami: ¿Aún puedes luchar, Madoka?
Madoka: ¡Sabes que sí!
Mami: ... Eres tan entusiasta como siempre.
Madoka: ¡Sí, lo soy!
Madoka: Quiero hacer lo que pueda.
Madoka: Quiero proteger Mitakihara y a todos mis@amigos que viven aquí.
Mami: Madoka...
Madoka: Y si todas hacemos lo que podemos...
Madoka: Poco a poco, todos nuestros esfuerzos se@irán acumulando.
Madoka: Siento que de esa manera podremos crear@un futuro mejor.
Madoka: Quiero decir, si Ui pudo volver a Kamihama...
Madoka: Y el sistema que nos mantiene a salvo de@convertirnos en Brujas sigue ahí...
Madoka: Es porque...
Madoka: Todas se esforzaron al máximo e hicieron lo@que pudieron.
Madoka: Por eso yo también voy a seguir haciendo lo@que pueda.
Mami: ... Sí, tienes razón.
Mami: Es importante alcanzar lo que podamos.
Kyoko: ¡Oigan! ¡Si están bien por allí, ayúdennos!
Sayaka: ¡Nosotras abriremos paso,@ustedes se encargan del resto!
Sayaka: [flashEffect:flashWhite2][surround2004A09]¡Hyah!
Kyoko: [flashEffect:flashWhite2][surround:2006A04]¡Rrraaahhh!
Mami: ¡Madoka, te seguiré el paso!
Madoka: ¡Muy bien! ¡Terminemos esto!
Mami y Madoka: [flashEffect:flashWhite2][se:5009_shooting_05_h][se:5003_shooting_02_vh]¡Haaaaaagh!
[se:6009_hit_shooting_05_h][se:6003_hit_shooting_02_vh]爾天人修　阿殊好阿　相時特難
Homura: ...
Homura: (Espera... Estamos trabajando en equipo.)
Homura: (Todas cumplen su papel,@pero nos ayudamos mutuamente...)
Homura: (Y cada una de ellas es más fuerte que antes...)
Homura: (Es por lo que pasó en Kamihama.)
Madoka: ¡Homura, date prisa!@¡El Laberinto está a punto de colapsar!
Sayaka: ¡Tenemos que sacar a esta gente de aquí!
Homura: ... ¡Está bien!
Mami: Todo listo, vamos.
Kyoko: Muévanse, muévanse.
