Momoko: ¡¿Qué?! ¡¿Las Magius están invocando@Walpurgisnacht?!
Kaede: ¡¿Qué es eso?!
Rena: ¿Es algún tipo de apodo para una Chica Mágica?
Momoko: Es una bruja.
Rena: ¡¿Qué?!
Momoko: De la peor clase, en realidad.
Kaede: ¡¿Quée?!, ¡Nooo! ¡¿Qué hacemos, Rena?!
Rena: ¡¿Cómo voy a saberlo?!
Yachiyo: Parece que han entendido lo esencial.@Tenemos problemas.
Yachiyo: Tendremos que detenerlas cuando nos encontremos@con Iroha.
Yachiyo: Kamihama podría ser completamente invadida.
Yachiyo: ¡¿Hay más de ellas?!
Pluma Blanca: Protegan esta zona a toda costa.
Pluma Negra: ¡S-sí! ¡No dejaremos que nos sobrepasen!
Yachiyo: Supongo que se les escapó alguna de las Plumas.@Qué terrible momento...
Kanagi: Puede que las otras Chicas Mágicas las mantengan@ocupadas...
Kanagi: Pero las órdenes de las Plumas son regresar a@su base de operaciones, así que...
Yachiyo: No tenemos tiempo para luchar contra ellas...
Mitama: Entonces, ¿por qué no me los dejas a mí?
Momoko: ¡¿Qu—  Coordinadora?!
Yachiyo: Mitama, tú...@¿Viniste para luchar en el frente?
Yachiyo: Espera, ¡¿Siquiera puedes luchar?!
Mitama: No hasta hace poco.@Pero he empezado a cogerle el truco.
Momoko: Aun así... ¿Estás segura de esto?
Mitama: He cambiado un poco de opinión.
Mitama: Esto es una emergencia, y no quiero seguir@estando al margen.
Yachiyo: ¿Estás segura de que podemos dejarlo en tus@manos?
Mitama: Sí, estaré bien. Tú adelantate.
Pluma Blanca: [flashEffect:flashWhite2][surround:7151A01][bgEffect:shakeLarge]¡No vas a ir a ninguna parte!
Pluma Blanca: [flashEffect:flashRed2][surround:1017A04][bgEffect:shakeLarge][chara:715100:effect_shake]¡Ugh... Ahh!
Mitama: Ustedes son las que no van a ninguna parte.
Mitama: Prepárate para que se agite bien tu Soul Gem si@pretendes luchar contra mí.
Pluma Negra: Eek...
