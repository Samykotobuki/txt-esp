Momoko: Huh, así que Mifuyu quiere hablarlo primero.@Eso suena como una buena idea.
Momoko: Aunque supongo que así es como deberían@hacerse las cosas.
Momoko: No es que alguna de las dos quiera@hacer daño a la otra.
Iroha: Eso es lo que dijo Mifuyu también.
Kaede: Sin embargo, ¿Será realmente seguro?
Kaede: No puedo creer que te inviten a@tener una charla con ellas...
Kaede: ¿No parecía antes que realmente iban@a hacerte daño?
Tsuruno: ¡No hay que preocuparse!
Tsuruno: ¡Tsuruno Yui estará allí! ¡Preparada@para romper en pedazos cualquier plan suyo!
Momoko: ¿Romperlo en pedazos?@¿Y si no se rompe tan fácilmente?
Momoko: Eh, supongo que no importa.
Momoko: Sé lo fuerte que eres, Tsuruno.@Si estás ahí, todo irá bien...
Tsuruno: Lo sé, ¡¿Verdad?!
Momoko: ¿Ya has averiguado dónde está?@¿Ese tal "[textRed:Museo de la Memoria]"?
Iroha: Bueno, aún no...@¿Alguna de ustedes sabe algo al respecto?
Momoko: En realidad, es la primera vez que@oigo hablar de eso.
Iroha: Lo imaginé...
Tsuruno: ¿Y tú, Kaede? ¿Rena?
Kaede: Um... No estoy segura...
Rena: Yo tampoco...
Rena: Pensé que ustedes eran las que estaban con@esa Rumorfilíca.
Tsuruno: ¿Te refieres a la Maestra Yachiyo?
Rena: Sí, ella.
Tsuruno: Nos dijo que nunca había oído hablar@de ella.
Iroha: Cierto, ella dijo que...
Rena: Oye, ¿Por qué esa cara larga?
Momoko: Bueno, supongo que tener una pista@no es lo mismo que saber dónde ir...
Momoko: Tiene sentido sentirse un poco deprimida.
Iroha: Ah, en realidad no es eso.
Iroha: Estaba pensando en Yachiyo.
Momoko: ¿Por qué? ¿Qué le pasa?
Iroha: Mifuyu ayer vino a visitarnos a casa.
Iroha: Yachiyo ha estado actuando muy extraña@desde que se fue... 
Momoko: [chara:101001:effect_emotion_surprise_0][se:7226_shock]¡¿Mifuyu fue a casa de Yachiyo?!
Iroha: Sí...
Iroha: Ha estado muy deprimida desde entonces... Aunque@quizás malhumorada sea una palabra mejor...
Momoko: Eso es duro...
Momoko: Quiero decir, Mifuyu es un punto muy@sensible para ella emocionalmente, así que...
Iroha: Aun así...
Iroha: Me gustaría que dejara de ocultar lo que le pasa.@Podríamos ayudarla si nos lo contara.
Momoko: Dejar de ocultarse, hm...
Rena: ¡Sí, Momoko!
Kaede: ¡Sí!
Momoko: [chara:101001:effect_emotion_surprise_0][se:7226_shock]*Asustarse*
Momoko: ¡Oye, Iroha!
Iroha: ¿Eh, sí?
Momoko: ¡Acabo de tener una gran idea!
Momoko: ¡¿Vendrías conmigo a ver a la Coordinadora@después de la escuela?!
Iroha: ¿Para ver a Mitama?
Iroha: ¡Buena idea!
Iroha: Ella podría saber algo sobre esto.
Momoko: Sí, eso es exactamente lo que estaba pensando.
Momoko: Me pidió que cazara Familiares para ella@hoy.
Momoko: Supongo que podemos pasar por la tienda@mientras estoy en ello.
Iroha: ¡Sí! ¡A mí también me encantaría ir!
Rena: Grrrr...
Kaede: Hnngh...
Momoko: ¡B-bueno, como sea!@Nos vemos después de la escuela...
