Walpurgisnacht: ¡Ah, ja, ja, ja! ¡Ah, ja, ja, ja, ja!
Alina: La escucho... El dulce risa de una bruja...
Alina: Me vuelve muy... muy excitada...
Touka: *Risita*
Touka: ¡Es la misma risa que escuchamos cuando observamos@al Incubador!
Touka: ¡Esto demuestra que Walpurgisnacht existe!
Nemu: Y también que el mundo que he creado no se@echará a perder.
Nemu: *Risita*
Touka: Oh, ¿eso significa que has terminado, Nemu?
Nemu: Sí.
Alina: Estaba de paseo conmigo y con las Plumas.
Nemu: Sí, pudimos tomar el control de todas las torres@de telefonía móvil.
Nemu: Tal y como has dicho, una torre estaba@configurada para controlar las demás.
Nemu: Todas las señales móviles en Kamihama están@siendo convertidas a una nueva frecuencia.
Nemu: Ahora deberían ser rayos visibles solo capaces@de ser vistos por las Brujas.
Touka: ¡Guau!
Touka: ¿Y, y?@¿Qué clase de mundo les estamos mostrando?
Nemu: El mundo que he creado es un paraíso para la@gente de Kamihama.
Nemu: Están libres de todas sus preocupaciones, y tienen@el mismo acceso a la comida y al refugio.
Nemu: También es rico en lugares y objetos que@satisfacen todos sus deseos.
Nemu: Sin embargo, no es perfecto.
Nemu: La gente debería estar nadando en el lujo,@libre de todo su sufrimiento.
Nemu: Pero empiezan a sufrir de nuevo cuando descubren@nuevas cosas con las que estar insatisfechos.
Nemu: En otras palabras, he creado una abertura para@que el paraíso se ponga patas arriba.
Nemu: *Risita*
Alina: Lo que está diciendo es que las Brujas ahora ven@a Kamihama como una utopía.
Alina: Es el lugar perfecto para ellas para extender@su maldición.
Alina: ¡Ah, ja, ja, ja!
Touka: ¡Eso es! ¡Las brujas querrán destrozar y@destruir todo!
Touka: *Risitas*
Touka: Bien, ahora es mi turno.
Touka: ¡Tengo que hacerla reír con más regocijo aún!
Alina: ¿Todavía hay más cosas que hacer?
Touka: ¡Ah, sí!
Touka: Voy a enviar una señal desde este observatorio@para estimularla emocionalmente.
Touka: Eso hará que atraerla sea aún más fácil.
Alina: No sabía que ibas a hacer eso.
Touka: Eso es porque no te lo he dicho.
Nemu: ¿Cómo lo vas a hacer?
Touka: ¡Utilizaré 1420,405751786 MHz!
Nemu: No te entiendo...
Touka: Se conoce como la línea de hidrógeno.
Touka: Es la onda de radio utilizada para recibir señales@de inteligencia alienígena.
Touka: Pasa por encima de todos los obstáculos y viaja@sin parar, cubriendo grandes distancias.
Touka: Incluso puede atravesar los cuerpos de las Brujas@para resonar con sus Grief Seeds.
Nemu: Así que por eso dijiste que la estimularía@emocionalmente.
Touka: Así es.
Touka: Haremos que Walpurgisnacht sea aún más violenta@usando estas ondas de radio.
Touka: Las Grief Seeds son originalmente Soul Gems que@han sido influenciadas por las emociones.
Touka: En su interior hay una energía similar a la de una@Célula iPS, que puede volverse en cualquier emoción.
Touka: ¡Están repletas de esa energía!
Touka: Así que vamos a estimularla y potenciarla.
Touka: Primero, emitiremos líneas de hidrógeno con ondas@cerebrales utilizadas en el hipnotismo.
Touka: Eso hará que el mundo creado por Nemu deje aún@más huella, ¿verdad?
Touka: Luego, aplicaremos las ondas cerebrales que se@emiten cuando el sujeto está excitado.
Touka: Eso cambiará las células emocionales iPS dentro@de la Grief Seed.
Touka: Se convertirán en células de rabia y excitación.
Touka: ¡Entonces todo lo que queda es que cargue@directamente contra Kamihama!
Touka: Es el plan perfecto, ¿verdad?
Nemu: Me hubiera gustado que me dijeras que era un plan@de dos etapas, pero seguro.
Touka: Oh, vamos.
Touka: Va a salir bien. ¡Eso es lo que importa!
Touka:¡ Muy bien, voy a empezar a emitir!
Nemu: Por favor, hazlo.
Alina: Ahh, estoy tan emocionada que mi corazón va a@estallar.... Ja...ajaja...
Touka: *Risitae*
Touka: ¡Aquí vamos!
