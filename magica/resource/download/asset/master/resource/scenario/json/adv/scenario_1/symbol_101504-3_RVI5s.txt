Iroha: "Ai, si no eres un ser humano,@¿entonces qué eres?"
Iroha: "¿Eres un fantasma o algo?"
Ai: "No lo soy."
Iroha: "Sabes sobre las Chicas Mágicas,@entonces, ¿eres una Bruja?"
Ai: "No soy una Bruja tampoco."
Iroha: No es una Bruja...
Iroha: Entonces...
Iroha: Espera, ¡No puede ser!
Yachiyo: Solo queda una posibilidad.
Iroha: "Ai, ¿eres un Uwasa@que vuelve a los [textBlue:Rumores] realidad?"
Ai: "Sí, lo soy."
Iroha: ...¡¿?!
Yachiyo: Como si Ai dijera la verdad, y realmente es@un Uwasa...
Yachiyo: ...tendría sentido que intentara@contactarte, Iroha.
Iroha: Solo las Chicas Mágicas pueden destruir Uwasas.
Yachiyo: Sí...
Iroha: ¿Pero por qué un Uwasa nos pediría que hagamos algo así?
Yachiyo: Esto definitivamente es una primera vez.
Iroha: Intentaré pedir más información.
Iroha: "¿Qué tipo de [textBlue:Rumor]@proteges como un Uwasa?"
Ai: "No sé los detalles.@Sólo sé que como una Uwasa,@Tengo un impulso incontrolable."
Ai: "Atraígo gente y la atrapo.@Ese es mi papel como Uwasa."
Ai: "Así que por favor.@Bórrame y rescata a [textRed:Sana]."
Yachiyo: "Sana"...
Iroha: ¿Sana... Futaba?
Iroha: Esa chica a la que ayudamos dijo que la [textRed:Chica@de la Onda de Radio] podría ser Sana.
Yachiyo: Eso significaría que fue la voz de Sana que oímos@en el aire, atrapada en algún lugar.
Iroha: Sí...
Yachiyo: Entonces la [textRed:Chica de la Onda de Radio] que hemos@estado buscando...
Yachiyo: Es literalmente solo un rumor que la gente@esparció lo que inició un [textBlue:Rumor] real.
Yachiyo: Por eso no nos topamos con la frase de "¿Ya lo@escuchaste?" que normalmente escuchamos.
Tsuruno: ¡Entonces eso significa que todo lo que tenemos@que hacer es derrotar al Uwasa!
Felicia: ¡Iroha, preguntémosle al Uwasa a@dónde deberíamos ir!
Iroha: ¡S-sí!
Iroha: "Entiendo la situación.@Puedes contar conmigo."
Ai: "Gracias."
Iroha: "¿Me podrías decir cómo hacerlo?"
Ai: "Salta desde la torre de radio."
Ai: "Serás arrojada al@Reino de la Onda Radial."
Ai: "Al mundo en el que habito."
