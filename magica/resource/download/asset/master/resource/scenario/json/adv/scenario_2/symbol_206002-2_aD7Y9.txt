Madoka: ¿Cómo va todo?
Sayaka: La Bruja está cerca.@Ya casi deberíamos estar allí.
Sayaka: ¿Cómo se está moviendo?
Madoka: Está dando vueltas como antes.@A la velocidad que va...
Madoka: No creo que salga de la zona que estimó Mami.
Sayaka: ¡Esa es nuestra Mami!
Homura: ¡Oh! ¡La Bruja cambió de dirección!
Madoka: ¡Tienes razón!
Madoka: Va en dirección al sol,@así que desde donde están ustedes...
Mami: Deberíamos dirigirnos al supermercado.
Kyoko: ¿El supermercado?@Este camino nos llevará más rápido.
Sayaka: ¡Oh, dulce trabajo en equipo!
Mami y Kyoko: ...
Kyoko: Sólo les dije un camino más rápido.@No es nada del otro mundo.
Mami: Lo sabemos.
Sayaka: ¡Lo logramos todas!
Madoka: ¡Sí, me alegro mucho!
Mami & Kyoko: ...
Homura: Um... Mami y Kyoko...
Madoka: ¿Soy yo o se ven aún más incómodas que@antes?
Sayaka: Bueno... No discutieron ni se pelearon.
Sayaka: Pero, no sé, es raro. Fue todo lo contrario.
Homura: ¿Lo contrario?
Sayaka: Es como si trabajasen muy bien juntas porque@eran muy cercanas en su día.
Sayaka: Como si conocieran las peculiaridades de@la otra.
Sayaka: Supongo que a Kyoko no le gusta eso.
Sayaka: Y Mami también parece sentirse rara al@respecto.
Madoka: Hmm... Suena complicado.
Mami: ¿Están listas las tres?
Kyoko: Si no vienen, las dejaré atrás.
Sayaka: ¡Espera, ya vamos!
Homura: ¿Va a ir todo bien?
Madoka: Estaremos bien en una pelea.@Podemos dar mucho apoyo.
Sayaka: ¡Por supuesto!@¡Bueno, acabemos con esta Bruja!
Homura: ... Está bien.
