Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4050_landing_jump]¡Hyah!
[flashEffect:flashRed2][se:6256_hit_spike_02_v][bgEffect:shakeLarge[name:sprite_0:effect_shake]｜?!ra(；ｘＴ)awr?!｜
Yachiyo: Ya nos encargamos del Uwasa.
Yachiyo: ¡Apresurémonos y encontrémonos con el equipo@de Momoko!
Mifuyu: Ugh...
Tsuruno: [chara:100300:effect_emotion_surprise_0][se:7226_shock]¡¿Mifuyu?!
Felicia: ¡Mujer de blanco!
Tsukasa: *Susto*
Tsukuyo: Oh, Yo... Yo...
Kanagi: ¡Tsukasa y Tsukuyo también!
Yachiyo: ¿Puede ser porque bajamos?
Kanagi: Es muy probable.
Kanagi: Probablemente las trajimos significativamente@más cerca a sus Soul Gems.
Yachiyo: Mifuyu, ¿Estás bien? ¿Sabes en dónde@estás?
Yachiyo: Soy yo
Mifuyu: ...¿Yachan?
Tsukasa: Kanagi... ¿V-viniste por nosotras?
Tsukuyo: ¿Viniste a rescatarnos?
Kanagi: Si. Es mi deber cuidar de ustedes.
Tsukasa: L-lo siento. Hiciste todo esto, luego de@que te traicionáramos así.
Kanagi: No te preocupes por eso. Está en el pasado.@¿Se pueden mover?
Tsukuyo: Si, estamos bien. Realmente sentimos mucho@todo lo que ha pasado...
Tsuruno: ¡Phew! ¡Que alivio que estén bien!
Tsuruno: Y ahora podemos tener de regreso a Mifuyu en@Mikazuki Villa
Tsuruno: ¡Sip, sip!
Mifuyu: Entonces ya saben que traicioné a las Magius...
Tsuruno: ¡Claro, ayudaste a todas a salvarme!
Tsukuyo: Siempre eras la que más se preocupaba por@nuestro bienestar, Mifuyu.
Tsukasa: ¡No las perdonaré por cómo te trataron!
Mifuyu: ¡Oh!
Mifuyu: ¡Yachan, Tsuruno! ¡Tendré que dejar los@detalles para después, pero!
Mifuyu: ¡Las Magius están haciendo algo verdaderamente@horrible!
Yachiyo: ¿Estás hablando de como invocaron a@Walpurgisnacht a Kamihama?
Mifuyu: ¡¿...?!
Mifuyu: Lo sabías...
Mifuyu: Acaso ya lo...
Yachiyo: Si. Ya lo hicieron.[chara:100601:motion_0][chara:100601:cheek_0][chara:100601:face_mtn_ex_050.exp.json]
Yachiyo: Usaron a las Plumas como una distracción@y durante el caos...
Yachiyo: ...ellas ejecutaron su plan sin problemas.
Mifuyu: No...
Tsukasa: ¿Walpurgisnacht? Mifuyu...
Tsukuyo: ¿Qué significa todo esto?
Mifuyu: ...
Mifuyu: Necesitan energía para incubar a Eve.
Mifuyu: Y para eso, han invocado a la peor Bruja@a Kamihama...
Mifuyu: Y usaron a todas las Plumas para hacerlo.
Tsukasa: No es posible.
Tsukuyo: Eso es terrible...
Tsuruno: ¡Por eso estamos aquí! ¡Para detenerlas!
Felicia: ¡Por supuesto!
Felicia: ¡Le daremos a esas Magius lo que@merecen!
Felicia: ¡Y mandaremos a esa Walpurgisnacht@de vuelta a casa!
Kanagi: Y viendo que tan lejos han llegado, queremos@ponerle un alto a todos sus planes.
Yachiyo: ¿Nos ayudarás, Mifuyu?
Kanagi: Si, serás de gran ayuda. Además, no creo que@quieras renunciar en este punto.
Mifuyu: Tienes razon.
Mifuyu: ¡Por favor déjemne ayudarlas!
Tsuruno: ¿A-ayudar?
Tsuruno: Si... *Solloza* ¡S-siiiiiiip!
Mifuyu: ¿Huh? ¿Tsuruno?
Tsuruno: [flashEffect:flashWhite2]¡Bienvenida a casa, Mifuyu!		  
Mifuyu: [flashEffect:flashWhite2][bgEffect:shakeLarge][chara:100601:effect_shake]Es bueno estar de regreso.
Yachiyo: *Solloza*
Yachiyo: Sabes que vamos a pelear. No llores así@en frente de Mifuyu...
Mifuyu: Lo que digas, Yachan...
Yachiyo: N-no estoy llorando... No aún
Yachiyo: Aún no es el momento. Puedo llorar cuando@todo esto termine.
Mifuyu: Tienes razon...
Tsukuyo: ¿Pero qué vamos a hacer con las Plumas@restantes?
Mifuyu: Quiero otorgarles la liberación a todas.@Sigo determinada a cumplir eso.
Mifuyu: Pero tenemos que ponerle un alto a lo@que las Magius están haciendo.
Mifuyu: Pensé que estaría bien si podía dirigirlas@en la dirección correcta...
Mifuyu: Pero perdí totalmente el control.
Mifuyu: Esto no se ve bien para el futuro de las@Chicas Mágicas.
Mifuyu: Qué van a hacer ustedes, ¿Tsukasa? ¿Tsukuyo?
Tsukuyo: ¡Ofrecer nuestra asistencia, claro!
Tsukasa: No importa si somos liberadas si no hay@un futuro más allá de eso.
Tsukuyo: Seguro.
Felicia: O-oh, ¡Hey! ¡Esas Plumas de por ahí!
Pluma Blanca: ...
Mifuyu: Están sosteniendo algo...
Mifuyu: No hay duda.
Mifuyu: ¡Esas chicas tienen nuestras Soul Gems!
Yachiyo: Se dirigen adentro.
Tsukasa: Eso es cerca de la entrada.
Tsukuyo: ¡D-debemos perseguirlas rápido!
