—Ciudad de Kamihama, Distrito de Sankyo—
Kyoko: (Sí, creo que aquí es donde estaba...)
Agua Milagrosa. Garantiza saciar tu sed@y rejuvenecer tu cuerpo cansado.
¿Le gustaría probar un vaso?
Kyoko: Aquí no hay nadie...@Quizá no esté abierto todos los días.
Kyoko: (Ah, hmm...)
Kyoko: (Ni siquiera puedo estar segura de si@la estación de agua tiene algo que ver...)
Kyoko: Bueno...
Kyoko: Probablemente debería volver@sobre mis pasos de ayer...
Kyoko: (Hay muchos estudiantes alrededor.)
Kyoko: (¿Tal vez pueda preguntarles sobre@la estación de agua?)
Kyoko: ...
Kyoko: [chara:200601:effect_emotion_surprise_0][se:7226_shock]¡¿...?! ¡¿Qué demonios?!
¿Ya lo escuchaste? ¿Quién te lo dijo?
Kyoko: Cuando me acerqué a la escuela,@vi una cosa extraña que murmuraba algo@una y otra vez como un poema.
Kyoko: La primera vez que lo vi, pensé que debía@ser un Familiar. Pero no tenía ningún tipo@de Laberinto a su alrededor. Además, cuanto@más miraba, menos se parecía a un familiar.
Kyoko: Es más... Había@un estudiante de pie junto a él,@charlando sin parar. Era bastante raro.
Kyoko: Si eso no es un Familiar,@¿entonces qué diablos es?
Disculpa, hay algo de lo que@me gustaría hablar contigo.
Kyoko: ¿Huh? Quién eres...
Yachiyo: Soy Yachiyo Nanami, una Chica Mágica.
Kyoko: Wow, ¿así que lo dices sin más?
Yachiyo: Tú también lo eres, ¿no?
Kyoko: Síp.
Kyoko: Soy Kyoko Sakura, y sí,@soy una Chica Mágica.
Kyoko: Bueno, ahora que se sabe todo esto,@¿qué es lo que quieres?
Kyoko: Esta no es la clase de ciudad en la que se atacan@unas a otras para robar territorio, ¿verdad?
Yachiyo: Eso es correcto.@No tengo intención de hacer nada tan inútil.
Yachiyo: Sólo estoy interesada en la criatura@que acaba de desaparecer.
Kyoko: Oh, ¿Esa cosa?@¿Qué fue, de todos modos?
Kyoko: No era un Familiar, eso es seguro.@Aparte de eso, no tengo ni idea.
Yachiyo: Ya veo, así que tú tampoco lo sabes.
Kyoko: Esa fue mi primera vez viendo uno.
Kyoko: ¡Ah! [chara:200601:lipSynch_0][freeze:1][chara:200601:cheek_0][chara:200601:face_mtn_ex_030.exp.json][chara:200601:lipSynch_1][chara:200601:effect_emotion_sad_0][se:7224_fall_into]Maldición, otra vez no...
Yachiyo: ¡¿...?![chara:100201:effect_emotion_surprise_0][se:7226_shock]
Yachiyo: ¿Ese papel...?
Kyoko: Oh sí, han estado cayendo de la nada@desde ayer. Regular como un reloj.
Yachiyo: ¿Por casualidad bebiste algo de agua?@¿De algún lugar con un cartel de búho?
Kyoko: Oh sí,[chara:200601:lipSynch_0][wait:1.0][chara:200601:cheek_0][chara:200601:face_mtn_ex_030.exp.json][chara:200601:lipSynch_1]bebí un poco. ¡Fue increíble!
Kyoko: ¿Pero cómo lo sabes?
Yachiyo: El agua podría ser la razón por la que@aparecen esos trozos de papel.
Kyoko: Según esta chica, Yachiyo Nanami,@estoy enredada en un Rumor que@se está haciendo realidad.
Kyoko: Sólo que resultó que no@sabía nada del rumor.
Kyoko: Sólo que cuando los números del papel@lleguen a cero, probablemente@ocurrirá algo malo...
Kyoko: Pero lo importante es que, los culpables de@convertir los Rumores en realidad son@unos monstruos totalmente diferentes@a las Brujas, llamados "Uwasa".
Yachiyo: Tengo una propuesta para ti. ¿Qué tal si@ investigas los [textBlue:Rumores] conmigo?
Kyoko: Nah, [chara:200601:lipSynch_0][wait:1.0][chara:200601:lipSynch_1]Estoy bien.
Yachiyo: ¡¿Por qué no?!
Kyoko: Es simple.
Kyoko: Estas "Uwasa" de las que hablas@no son Brujas.
Kyoko: Si lo fueran, bueno, podríamos@hacer negocios, sin problemas.
Kyoko: Cuando derrotas a una Bruja, la gente que@estaba controlando vuelve a la normalidad.
Kyoko: Pero las Uwasa no son nada@como las Brujas, ¿verdad?
Kyoko: Por lo tanto, no hay garantía de que puedas@salvar a todos como con una Bruja.
Kyoko: Ya ves, eso no me pone de humor@para jugar a las amigas contigo.
Kyoko: Desearía poder pagarte por la información,@pero no quiero poner mi vida en juego.
Kyoko: Lo siento. Me iré ahora.
Yachiyo: ¡Espera, Kyoko!
Kyoko: (¿Hacer equipo para poder sobrevivir?@Lo siento, no me interesa.)
Kyoko: (Digamos que termina con una de nosotras@dando la vida para salvar a la otra.)
Kyoko: (Si algo así ocurriera,@es imposible que no me enfadara.)
Kyoko: (Una vez que se llega a ese punto,@es demasiado tarde para lamentarse.)
Kyoko: Por eso siempre seguiré siendo una solitaria.
