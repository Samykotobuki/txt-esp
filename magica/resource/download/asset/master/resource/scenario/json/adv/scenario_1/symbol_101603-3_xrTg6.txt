Touka: Bien, ¿Se sienten un poco más tranquilas?
Touka: Cuanto más ansiosas estén, más difícil@será para mí hacer la conferencia.
Tsuruno: Iroha... ¿Estás bien?
Iroha: Sí, estoy bien...
Iroha: He aceptado que ella podría no ser la@Touka que conozco...
Iroha: No sirve de nada deprimirse por ello.
Iroha: Todos se han olvidado de Ui.
Iroha: Sería demasiado conveniente que Touka@fuese la única que la recordara...
Iroha: Voy a concentrarme en la conferencia@por ahora...
Touka: Me alegro de que lo veas así.
Touka: Supongo que podemos empezar entonces.@¿Están todos de acuerdo con eso?
Felicia: Es aburrido sentarse aquí y esperar.@Si vas a empezar, entonces empieza.
Tsuruno: Lo mismo digo. Soy toda oídos.
Sana: Sí, cuando estés lista...
Touka: ¡Maravilloso! ¡Empecemos la conferencia!
Touka: El tema de nuestra conferencia de hoy es simple...@¡"La verdad detrás de las chicas mágicas"!
Touka: Todos los involucrados en las Alas de@Magius ya saben todo esto.
Touka: Sin bromas, sin mentiras. ¡Solo la verdad!
Touka: Ahora escucha con atención, voy a empezar con@una historia.
Iroha: [chara:100101:lipSynch_1][chara:100101:voiceFull_fullvoice/section_101603/vo_full_101603-3-20]Okay...
Touka: Artículo 1: Soul Gem.
Touka: Había una vez tres Chicas Mágicas.@A, B y C. Eran buenas amigas.@Cazaban Brujas juntas.
Touka: Pero un día, dejaron escapar a una bruja@porque era demasiado difícil de derrotar.@Pero luego, se encontraron con esa misma bruja otra vez.
Touka: ¡Ah, pero en qué lío acabaron!
Touka: Parecía que las tres iban a caer ante@el poder abrumador de esa Bruja.
Touka: Entonces, en un momento crítico, C se@levantó y cargó contra la Bruja, sola.
Touka: Gracias a ella, el punto débil de la bruja@quedó al descubierto, y A pudo acabar con ella.
Touka: Sin embargo, C murió, sin que se encontrara@una sola herida en su cuerpo.
Touka: Ahora, ¿cómo crees que sucedió esto?
Iroha: Oh. Así que esta conferencia tiene un formato@de concurso.
Touka: Me gustaría que al menos intentaras pensar.
Sana: Er, así que no fue herida... en absoluto...
Felicia: ¿Se envenenó o algo así?
Touka: ¡Bzzzt! Incorrecto.
Iroha: ¿No podía respirar?
Touka: ¡Nop! ¡Qué pena!
Iroha: Bien...
Tsuruno: ¿Era su Soul Gem?
Touka: ¡Correcto! Realmente eres la más poderosa, ¿eh?
Iroha: ¿Su... Soul Gem?
Touka: Sí. Deja que te explique...
Touka: Junto al cuerpo ileso de C yacía su@Soul Gem, hecha pedazos.
Touka: Fue entonces cuando A y B se dieron cuenta de que una@Soul Gem es, tal y como su nombre indica,@la vida de una Chica Mágica.
Touka: ¡Esa es la respuesta correcta!
Sana: Esta Soul Gem es... ¿mi vida?
Touka: No lo parece, ¿verdad?
Sana: En absoluto...
Touka: Entonces intenta sacar tu Soul Gem por mí.
Sana: Um, vale...
Sana: ¡Augh...!
Iroha: ¡Sana!
Touka: Lo siento por eso. Pero, ahora lo entiendes,@¿no?
Touka: Incluso si te golpean con un pequeño ataque mágico@como ese...
Touka: El daño es suficiente para que te retuerzas@como un bebé.
Touka: Es lógico que mueras si tu Soul Gem@se rompe, ¿verdad?
Sana: Ugh... Si... si tu Soul Gem es tu vida...
Sana: Eso significa que cuando usas la magia...
Touka: ¡Oh, muy bien! ¡Te has dado cuenta, Chica Invisible!
Touka: Cuando usas magia, estás literalmente tirando@rebanadas de tu propia fuerza vital.
Touka: ¿Te has sentido así alguna vez?
Iroha: Tirando... tu propia vida...
Iroha: Lentamente... Lentamente...@Mis entrañas comienzan a congelarse...
Iroha: Siento que mi conciencia va a ser absorbida@por ese pozo en mi corazón...
Yachiyo: ¡¡Iroha!!
Yachiyo: ¡Quédate con nosotros!
Iroha: Tengo...
Touka: Esa sensación es básicamente una prueba de que@te has quedado sin baterías.
Iroha: ¿Qué me he quedado sin baterías...?
Touka: Quiero decir, solo estoy diciendo las cosas como son.
Touka: Tu Soul Gem es tu fuerza vital con@forma física fuera de tu cuerpo.
Touka: Algo así como sacar la batería interna@de un aparato y usarla externamente.
Touka: Si no hicieras eso, sería difícil comprobar@tu estado durante la batalla, ¿verdad?
Sana: En ese caso, ¿las Grief Seed son...@como cargadores de bateria?
Touka: En realidad, son más bien limpiadores. Pero sí,@supongo que funciona para esta analogía.
Touka: Las baterías pueden usarse una y otra vez simplemente@mediante la oxidación y la reducción.
Sana: ¿Qué significa eso?
Touka: Si usas una batería, su estructura química cambia,@haciéndola inutilizable.
Touka: Pero si la cargas de nuevo, su estructura química@vuelve a su estado original.
Touka: Si usas una Soul Gem, cambia la magia@interior y la hace impura.
Touka: Usar una Grief Seed revierte ese efecto.
Touka: Entonces, ¿no dirías que eso hace que una@Soul Gem sea como una batería?
Touka: ¿No es fascinante?
Sana: No creo que sea fascinante en absoluto...
Sana: Eso... eso significaría... que ya no soy realmente@humana, ¿verdad?
Touka: Bueno, un humano moriría si luchara contra una@bruja. Tú no lo harías, ¿verdad?
Touka: Así que según ese criterio, no, no eres humana.
Sana: Pero...
Touka: Además, ¿el hecho de creerse@humano no te hace humano?
Touka: En la medicina actual...
Touka: Usamos la química, la física, la biología@y la ingeniería electrónica...
Touka: Desarrollamos todo tipo de tecnología a partir@de diversas ciencias, y...
Touka: Luego usamos esa tecnología para modificar nuestros@propios cuerpos, ¿no es así?
Sana: Sí, pero...
Touka: La gente modifica sus cuerpos todo el tiempo,@y tú estás dispuesta a aceptarlo.
Touka: ¿Y sin embargo no aceptas lo que te estoy diciendo?@Eso es raro.
Sana: ...
Touka: Lo estás pensando demasiado.
Touka: Después de todo, si ninguno de nosotros es humano.@¿En qué se convierte una chica invisible como tú?
Sana: ¡¿...?!
Sana: Ese es un buen punto...
Iroha: Si lo que dices es cierto, Touka, entonces eso@significa que... casi muero en ese entonces.
Iroha: Esa debió ser la razón por la que Yachiyo@estaba tan aterrorizada...
Touka: Hay otra razón detrás de ese terror.@Lo entenderás más adelante en la historia.
