Touka: Hmm, ¿Cómo puedo hacer que me creas?
Touka: Ah, lo sé.
Touka: Resulta que tengo una Grief Seed conmigo.
Touka: Señorita Mercenaria, ¿Podrías prestarme tu@Soul Gem un momento?
Felicia: ¡¿Eh?! Ni hablar. ¡La vas a romper!
Touka: No hay por qué tener miedo.
Felicia: ¡¿Eh?! ¿Quién tiene miedo?
Touka: Entonces, ¿Te parece bien entregarla?
Felicia: Ugh...
Touka: Te molestó bastante nuestra charla sobre las@brujas, ¿Verdad?
Touka: Así que pensé que debías de tener muchas@impurezas almacenadas ahí dentro.
Felicia: ¿Eh? ¿Gracias... a ti, no?
Iroha: ¿Esta es otra explicación?
Touka: Por supuesto. No te daría una Grief seed@sin una buena razón.
Iroha: Así que, estás diciendo que la razón por la que@una Grief Seed pueda purificar una Soul Gem...
Iroha: ...es porque las Chicas Mágicas y las Brujas son lo@mismo. Al igual que las maldiciones y las impurezas.
Iroha: ¿Es eso lo que quieres decir?
Touka: Eso es correcto, Iroha Tamaki.
Touka: Lo que quiero decir es que no hay nada que pueda@sustituir a las Soul Gem y a las Grief Seed.
Touka: Este intercambio de impurezas solo ocurre entre@estas dos cosas específicas.
Touka: Este tipo de reacción es similar a lo que vemos@en los genes de los seres vivos, ¿verdad?
Tsuruno: ¿Una reacción genética... como el mestizaje?
Touka: ¡Realmente eres la más poderosa!
Touka: Las especies con estructuras genéticas diferentes@no pueden cruzarse.
Touka: Puedes crear un "Ligre", pero esa criatura@es imposible en el mundo natural.
Tsuruno: Entonces lo que estás diciendo es que...
Tsuruno: Así como los organismos pueden crear descendencia@porque sus estructuras genéticas coinciden...
Tsuruno: Las Soul Gem y las Grief Seed coinciden, y es@por eso que el proceso de purificación es posible.
Tsuruno: Así que ni siquiera estás diciendo que son similares,@estás diciendo que son esencialmente lo mismo.
Touka: Eres tan aguda como dijeron que eras.
Touka: Los detalles son diferentes, pero has acertado@en lo general...
Touka: Me lo has explicado todo.
Tsuruno: Um, ¿gracias?
Tsuruno: Pero aunque entienda la lógica que hay detrás,@sigue siendo difícil aceptarla...
Tsuruno: La razón por la que Felicia está tan afectada es@porque...
Tsuruno: Si estamos destinadas a convertirnos en brujas, eso@significa que hemos estado matando a chicas mágicas.
Tsuruno: ¿Cómo se puede aceptar eso?
Touka: Yo diría que esa es solo una forma de verlo.
Tsuruno: ¿Qué quieres decir?
Touka: Podrías pensar en las Brujas como si fueran zombis,@como en una película de terror.
Touka: Eran humanas, pero han perdido todo el sentido de@sí mismas. Hacen cosas malas, se multiplican...
Touka: Así es como he elegido pensar en ellas.
Touka: Cuando matas a una bruja, es como si estuvieras@evitando que empeoren sus pecados.
Sana: Ni siquiera soy humana... Mi futuro está fijado en@piedra... Me convertiré en una Bruja...
Sana: ¿Y Estás diciendo qué no tenemos otra opción que@aceptar eso como un hecho...?
Iroha: Sana...
Iroha: Pero en el Santuario de Reunión,@no me convertí en una bruja...
Touka: Sí. Eso nos lleva a nuestro siguiente tema.
