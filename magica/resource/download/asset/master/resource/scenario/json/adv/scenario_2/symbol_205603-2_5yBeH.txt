Sayaka: Pateamos y golpeamos,@y logramos alejarnos.
Sayaka: Atravesamos la ciudad sin mirar@siquiera por dónde íbamos...
Sayaka: Estaba en pánico. Lo único en@lo que podía pensar era en huir.
Madoka: ¡D-detente! ¡Sayaka, espera!
Sayaka: ¡¿Qué?! ¿Qué pasa, Madoka?
Madoka: *Jadeo* *Jadeo*
Madoka: Creo que ya estamos lo suficientemente lejos...
Homura: S-sí, probablemente... *Jadeo*
Sayaka: Oh...
Sayaka: E-está bien.
Madoka: *Exhala*
Madoka: ¿Ahora puedes decirnos qué pasó, Sayaka?
Madoka: ¿Qué es lo que pasó allí?
Sayaka: ...
Sayaka: (Ellas necesitan saber...)
Sayaka: (¿Pero cómo debo se los digo?)
Homura: ¿Sayaka?
Sayaka: ...
Sayaka: (¡Tengo que decirles!)
Sayaka: Respiré hondo y me armé de valor.@Les conté todo lo que había visto y oído,@mientras lo ordenaba todo en@mi cabeza al mismo tiempo.
Madoka: ...
Sayaka: ...
Homura: ...
Homura: (Ha sucedido... Finalmente llegaron@a la verdad.)
Homura: (Madoka, Sayaka...)
Sayaka: Uh, Madoka...
Madoka: ¿Nos convertiremos en Brujas?
Homura: Madoka...
Madoka: Y Mami...
Sayaka: Madoka...
Madoka: ¡¿Por qué?!
Madoka: ¡¿Cómo pudo pasar esto?!
Madoka: ¡Sayaka! ¡Homura!
Homura: ¡Madoka! ¡Cálmate!
Madoka: ¡Esto es horrible! Demasiado horrible@como para describirlo con palabras...
Sayaka: Realmente lo es...
Sayaka: ¡¿Por qué tuvo que ser así?!
Sayaka: Gritamos, y nuestras voces@fueron simplemente tragadas en@la oscuridad del cielo nocturno.
