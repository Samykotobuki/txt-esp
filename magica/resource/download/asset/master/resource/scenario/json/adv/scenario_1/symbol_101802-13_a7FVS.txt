Tsukuyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1018A05]¡Vrrr!
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4050_landing_jump]¡Tendrás que hacerlo mejor que eso!
Pluma Negra: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:715010A01]¡Grrrghhh!
Yachiyo: ¡¿...?!
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge]Las otras Plumas-
Yachiyo: [flashEffect:flashRed2][surround:715010A02][bgEffect:shakeLarge][chara:100200:effect_shake]¡Aghhh!
Yachiyo: Ugh...
Yachiyo: (Siguen llegando...)
Yachiyo: No puedo creer que perder la consciencia les haya@hecho mucho más difíciles de combatir.
Yachiyo: Quizás debería haber tomado a estas hermanas más@en serio...
Tsukuyo: ...
Tsukasa: ...
Yachiyo: Antes dijiste que te parecería bien morir mientras@todas fueran libres...
Yachiyo: ¿Has olvidado cómo te sentías entonces?
Tsukuyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1018A05]Flute Blossom...
Tsukasa: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1019A04]...Resonance.
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge]¡¿...?!
Yachiyo: Como sospechaba, parece que no entrarán en razón@a menos que mate al [textBlue:Uwasa].
Kanagi: Realmente te has rebajado a convertirte en una@verdadera villana, utilizando peón tras peón.
Kanagi: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1016A01]¡Pero tu juego termina aquí!@¡Jaque mate!
Pluma Negra: [flashEffect:flashRed2][bgEffect:shakeLarge][surround:1016A05][chara:715001:effect_shake]Agh...nghhh...
Kanagi: *Suspira*
Kanagi: Por fin... Puedo enfrentarme a la evangelista sin@que me acorralen sus seguidores.
Mami: ¿Cómo puedes llamarme villana?
Mami: Solo estoy haciendo lo mejor para salvar a todas.
Kanagi: ¿Y usar peones para agotarnos antes de enfrentarnos@a ti es parte de eso?
Mami: Lo es.
Mami: No es el momento de preocuparnos por el orgullo@mezquino luchando una a una.
Kanagi: Me pregunto... ¿Realmente crees eso?
Kanagi: ¡Me temo que yo también tendré que leer tu mente!
Kanagi: ¿Qué es esto...?
Mami: *Risita* Mis sentimientos eran completamente@genuinos, ¿no es así?
Kanagi: Sí, ahora entiendo que no hay lugar para que@razonemos contigo.
Kanagi: En otras palabras...
Kanagi: ¡No tengo más remedio que obligarte a someterte!
