Iroha: ...
Iroha: El [textBlue:Rumor] rumor de la Sakura Eterna@debe estar por aquí...
Iroha: Yachiyo...
Yachiyo: No estoy dudando de ti.
Yachiyo: Debe ser difícil encontrar un [textBlue:Rumor] en@las montañas.
Yachiyo: Y si no se difunde intencionadamente,@por supuesto que no sabríamos está.
Yachiyo: ¿Cuál es tu plan?
Yachiyo: Las Magius dijeron que eliminarán todos@los [textBlue:Rumores].
Iroha: Pero el [textBlue:Rumor] de la Sakura Eterna era especial.
Iroha: No creo que Nemu sea capaz de destruirlo.
Iroha: Por eso lo pregunto.
Iroha: ¿Puedo ir a investigarlo?
Iroha: Nadie sabe lo del [textBlue:Rumor] de la Sakura Eterna.
Iroha: Si realmente está ahí, entonces sería el escondite@perfecto para su base.
Iroha: (La forma en que las plumas están entrando@y saliendo...)
Iroha: (Debe estar vinculado a su base, de alguna manera.)
Iroha: (Si podemos tomar el control de la zona mientras@las Plumas no están...)
Iroha: (Entonces podremos reunirnos con Yachiyo y el@resto de la banda aquí).
Iroha: Por favor, Sana...
Sana: ...
Sana: ¡¿Yo?!
Yachiyo: Sí. Eres la única que puede ocultar tanto tu magia@como a ti misma.
Yachiyo: Protege a Iroha, por favor.
Sana: Soy... la única que puede proteger a Iroha...
Sana: ¡E-está bien!
Sana: Haré todo lo que pueda para mantener a Iroha@a salvo... ¡Ah!
Sana: A-ay, me mordí la lengua.
Yachiyo: *Risita* De todos modos, contamos contigo, Sana.
Sana: (Traicioné a Iroha y su amabilidad...)
Sana: (Y rompí mi promesa a Ai.)
Sana: (Así que tengo que hacer esto.)
Sana: (Me salvaron tantas veces y me trataron como@a una verdadera amiga).
Sana: (¡Tengo que recompensarles!)
Sana: (Esta vez haré feliz a Iroha.)
Sana: (La ayudaré, y...)
Sana: ...
Sana: Las Plumas han dejado de entrar y salir.
Sana: ¿Se habrán ido...?
Sana: ...
Sana: ¡Sí!
Sana: ¡Iroha!
Iroha: ¿Qué te parece, Sana?
Sana: ¡Creo que todas se han ido!
Sana: ¡Entremos!
Iroha: Muy bien, gracias.
Iroha: ...
Iroha: Espero que las demás estén bien...
Sana: Sí, hace tiempo que no podemos contactarlas...
Iroha: Pero en momentos como este es cuando más@importante es tener fe en ellas.
Sana: Sí... Entremos y aseguremos la zona para poder@esperarlas allí.
Iroha: De acuerdo.
Sana: Muy bien entonces. ¿Me tomas de la mano?
Iroha: ¿Tu mano?
Sana: Así puedo ocultar la magia de ambas.
Sana: Si alguien sigue dentro del [textBlue:Rumor]@podría percibirnos.
Iroha: ¡Buen punto! Gracias.
*Squeeze*
Iroha: Parece que no hay nadie.
Sana: Sí.
Sana: Iroha... ¿Es este el [textBlue:Rumor] de la@Sakura Eterna?
Iroha: Nunca lo he visto, así que no puedo asegurarlo.@Pero, ¿Quizás?
Sana: Ah... Ese gran árbol de allí...
Sana: Ahora mismo no está floreciendo, pero es un árbol@de sakura...
Iroha: Entonces este debe ser el [textBlue:Rumor] de la@Sakura Eterna.
Iroha: Así que Touka y Nemu realmente no pudieron@deshacerse de él.
Iroha: Parece que no hay nadie aquí.
Iroha: Puede haber una forma de entrar en su base en@algún lugar de por aquí.
Iroha: Busquemos en la zona.
Sana: ¡Bien!
*Se sueltan de manos*
Sana: (Ah, su mano... Qué pena...)
Sana: Espera, ¿acabo de sentir algo de magia? ¡Iroha!
Iroha: Hay alguien aquí.
Iroha: Esta magia...
Nemu: ...
Touka: Iroha Tamaki.
Iroha: Touka... y Nemu.
