Tsukasa: No podemos... dejar que termine así...
Tsukuyo: Esta tarea nos fue encomendada@por las tres Magius.
Tsukuyo: No la abandonaremos tan fácilmente...
Kyoko: ¿Qué?@... ¡Tch!
Kyoko: Nos encargamos de esa payasa@de antes sin problemas, pero...@Aquí hay más payasas para@bloquear nuestro camino de nuevo.
Kyoko: Me imaginé que podríamos@superarlas como hicimos con@las anteriores, pero...
Iroha: Tienen que parar.
Iroha: Sus Soul Gems... Está casi sin magia...
Tsukuyo: *Risas* Las condiciones son perfectas...
Yachiyo: ¿En qué mundo es buena una@Soul Gem manchada?
Yachiyo: Espera... ¿Podrían estar...?
Tsukuyo: Esta puede ser una gran oportunidad@para todas ustedes.
Tsukasa: Verán por qué Kamihama es un@[textRed:lugar de liberación].
Tsukuyo: ¡Y podrán presenciarlo con sus propios ojos!
Kyoko: Después de celebrar cuán oscuras estaban sus@Soul Gems, una "cosa" espeluznante@y extraña apareció de sus cuerpos.
Kyoko: ¡¿Qué...ééé?!
Kyoko: Oye, espera... espera un segundo...@¡¿Qué demonios es esto?!
Yachiyo: Esa "cosa" nos salvó a todas cuando salió@de Iroha la última vez.
Yachiyo: Pero luchando contra eso, realmente puedes@sentir lo poderoso que es...
Kyoko: ¿Algo como ESO salió de ti también?
Iroha: S-sí...
Mami: Kyoko, alguna vez[chara:200501:lipSynch_0]... 
Mami: ¿Te has encontrado con una bruja que@pudiera hacerse pasar por humana?
Kyoko: Ajá...
Kyoko: Tú debes ser esa "Bruja Humanoide"@de la que hablaba Mami...
Kyoko: Rayos, ahora si que me doy una idea@de lo peligrosa que es esa cosa...
Tsukuyo: Ahora, si quieres pasar por nosotras@deberas poner tu vida en juego.
Kyoko: ¡Tch!
Kyoko: Qué considerado. Incluso cuenta@los minutos para nosotras.
Kyoko: Oh vaya, esto se está poniendo@realmente mal ahora...
Kyoko: Si estos rumores realmente se hacen@realidad, tendré unos 30 minutos antes@de que el desastre caiga sobre mí.
Kyoko: No puedo permitirme el lujo de@estar atrapada aquí. Todavía no@he visto cómo es el monstruo.
Iroha: Um, sí, por favor.
Kyoko: ¿Puede darme un vaso también?
Felicia: ¡Deme otro vaso!
Kyoko: Fuimos nosotras tres las que bebimos el@agua y quedamos atrapadas en este Rumor.
Kyoko: Así que, como víctimas de este rumor,@las tres decidimos dirigirnos@directamente a la Uwasa mientras@las demás se ocupaban de las locas.
Kyoko: Estaba pensando en dejarlas atras@y vencer a este Uwasa yo misma...
Kyoko: Pero vamos a tener que atacar las tres@para tener una oportunidad.
Iroha: ¿Ibas a "dejarnos atras"?[chara:100100:lipSynch_0][freeze:1.0][chara:100100:cheek_0][chara:100100:face_mtn_ex_050.exp.json][chara:100100:motion_400][chara:100100:lipSynch_1]@¡¿En serio estabas pensando en eso?!
Kyoko: Las Uwasa no son como las Brujas, ¿verdad?
Kyoko: Así que no puedes estar segura de que todas@terminaran bien después de que derrotes uno...
Felicia: ¡Sí, tiene razón!
Kyoko: ¿Verdad?
Kyoko: Pero no tenemos tiempo para preocuparnos@de eso ahora.
Iroha: ¿Quieres decir...?
Kyoko: Sí, en este punto...
Kyoko: Odiaba la idea de que todas las@demás se adelantaran a mí y me dejaran@atrás en el polvo.
Kyoko: Quería dejarlas atrás, y seguir adelante y@eliminar a la Uwasa por mi cuenta...@Pero ya he pasado el punto en@el que puedo decirles esa clase de basura.
Kyoko: Quiero apostar por la posibilidad de que salgamos@victoriosas, en lugar de morir aquí en vano.@Y la mejor manera de hacer eso es usar@toda nuestra fuerza para enfrentarnos a esa cosa.
Kyoko: No importa quién se salve al final.@¡Sin arrepentimientos!
