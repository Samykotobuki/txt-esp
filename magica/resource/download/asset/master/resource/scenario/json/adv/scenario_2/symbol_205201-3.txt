Mami: Entonces...@Sobre lo que dijiste antes.
Kyoko: ¿Huh?
Mami: Viniste hasta aquí buscando una Bruja.
Mami: Eso significa...
Mami: Que no había brujas en Kazamino, ¿verdad?
Kyoko: Bueno... no sé si no hay ninguna...
Kyoko: Pero al menos...
Kyoko: Yo no he visto ninguna en Kazamino.
Mami: Ya veo...
Kyoko: Las cosas tampoco se ven tan diferentes aquí.
Kyoko: Esa bruja fue probablemente la primera@y la última.
Mami: Eso parece. Lo que significa que@esta ciudad esta...
Mami: Igual que Mitakihara...
Kyoko: ¿Mitakihara?@[chara:200601:lipSynch_0][freeze:1.0][chara:200601:motion_1:cheek_0:face_mtn_ex_000.exp.json][chara:200601:lipSynch_1]¿Estás diciendo que no hay brujas allí?
Mami: Bueno, como dijiste antes...
Mami: No puedo asegurar que no quede@absolutamente nada...
Mami: Aun así, hubo un claro descenso@en sus números recientemente.
Kyoko: ¡Esto realmente apesta!
Kyoko: ¿Qué voy a hacer sin algo que cazar?
Kyoko: ¡El horno está prendido, pero no hay carne!
Mami: (Espero que esto sea algo que podamos@dejar pasar, pero...)
Mami: (El agotamiento de nuestro suministro de@magia puede no ser el único problema...)
Mami: (Las brujas desapareciendo tan@repentinamente...)
Mami: (No puedo evitar pensar que todo es@un poco sospechoso.)
Mami: (¿Esto solo está ocurriendo en ciudades@cercanas como Mitakihara y Kazamino?)
Mami: (O...)
Kyoko: ...
Kyoko: ¿Pensando en algo otra vez?
Kyoko: Si algo realmente malo está pasando...
Kyoko: Te ayudaré.
Kyoko: Después de todo, tendremos problemas@si no podemos conseguir Grief Seeds.
Kyoko: Tanto tú como yo...
Kyoko: Veré si puedo encontrar algo sobre la@desaparición de las brujas y esas cosas.
Kyoko: Te informaré si lo hago.
Mami: Está bien.
Mami: (¿Es todo esto algún tipo de coincidencia?)
Mami: (Tal vez hay alguna razón detrás.)
Mami: (Voy a tener que investigar esto.)
