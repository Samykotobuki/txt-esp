Iroha: ...
Iroha: U-um. Yo...
Iroha: ...
Iroha: *Susto*
Iroha: (¿Qué pasó?)
Pluma Negra: No te muevas.
Iroha: ¡¿P-plumas Negras?!
Iroha: ¡N-no me puedo mover!
Pluma Negra: Estás despierta. Eso significa que tu Soul Gem@está cerca.
Pluma Negra: Te llevamos a con las Magius.
Iroha: ...
Iroha: (Podré acercarme a mi Soul Gem si me@quedo con ellas.)
Iroha: (Sólo necesito recuperarla, entonces puede que@tenga posibilidad de escaparme.)
Pluma Negra: I pensé que intentarías escapar.
Iroha: No hay nada que pueda hacer por mi misma si no me puedo@transformar.
Pluma Negra: En efecto, tienes razón.
Pluma Negra: No hay forma de que escapes, salvo@un milagro.
