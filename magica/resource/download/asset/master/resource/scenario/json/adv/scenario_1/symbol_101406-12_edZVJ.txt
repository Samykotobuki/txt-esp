Mifuyu: Tiempo sin vernos, Yachan.
Yachiyo: ¿Mifuyu...?
Iroha: (Ella es la persona que Yachiyo@estaba buscando....)
Tsuruno: ¡M-Mifuyu!
Tsuruno: ¡¿Dónde has estado?! ¡Te buscamos@por todas partes!
Tsuruno: ¡VEN AQUÍ!
Mifuyu: ¡Wah...!
Mifuyu: Tsuruno, por favor, no te lances@así sobre mí.
Tsuruno: ¡Esto merece una fiesta! ¡Hoy es el día@que recuperamos a Mifuyu!
Mifuyu: Recuperamos... Hmm...
Tsuruno: ¡Si! ¡Volviste con nosotras!@¡Tenemos que celebrar en Banbanzai!
Mifuyu: Lo siento, pero no podré ir.
Tsuruno: ¿Huh? ¿Por qué?
Mifuyu: No puedo volver a tu equipo, Tsuruno.
Tsuruno: ¡No bromees con eso!
Mifuyu: Debes entender la razón por la que apareci@ahora mismo.
Tsuruno: Pero no puede ser...
Mifuyu: Me temo que sí.
Mifuyu: Soy una de las [textRed:Alas de Magius].
Tsuruno: ...¡¿?!
Tsuruno: Entonces, eso significa que... ¿eres nuestra enemiga?
Mifuyu: Si insistes en eliminar a los Uwasa,@entonces... sí, soy tu enemiga, Tsuruno.
Tsuruno: N-no...
Tsuruno: ¡Yachiyo!
Yachiyo: ...
Yachiyo: También quiero creer que está mintiendo.
Yachiyo: ¿De verdad eres Mifuyu...?
Mifuyu: Veo que has visitado el Santuario Séanse.@Has pasado por mucho para encontrarme...
Mifuyu: Y eso significa que debes haberte encontrado@al falso yo que estaba ahí.
Mifuyu: Me... alegra.
Mifuyu: Es un honor tener una mejor amiga@que me buscaría tanto.
Yachiyo: ¿Por qué debería escucharte?
Yachiyo: Debes ser falsa, justo como la otra...
Mifuyu: ...
Mifuyu: No estamos dentro de un [textBlue:Rumor].
Mifuyu: Por lo tanto, todo lo que se ve es completamente@real.
Yachiyo: Yo...
Yachiyo: ¡Yo no quería esto!
Yachiyo: ¡Yo no quería volver a verte así!
Yachiyo: Pero no es demasiado tarde. Por favor,@Mifuyu, ¡solo vuelve con nosotras!
Mifuyu: No puedo...
Mifuyu: Tengo un papel importante dentro de las@[textRed:Alas de Magius].
Mifuyu: Actúo el vínculo entre las Magius@y las Chicas Mágicas que las siguen...
Yachiyo: Pero lo que predican no puede ser@más que una mentira...
Mifuyu: Aun así, quiero poner mi fe en eso.
Yachiyo: ¿En la liberación?
Mifuyu: Sí...
Yachiyo: ¿No habías decidido vivir tu vida como una@Chica Mágica hace mucho tiempo?
Mifuyu: Yachan, ¡eres la única que todavía@se siente así!
Yachiyo: ...¡¿?!
Mifuyu: Por favor deja esto así.
Mifuyu: No voy a cambiar de opinión,@sin importar que digas.
Yachiyo: Así que estás dispuesta a lastimar gente,@solo para ser salvada...
Yachiyo: ¿Esto es por lo que pasó hace un año...?
Mifuyu: Sí, así es...
Mifuyu: Eso te... cambió, Yachan...
Yachiyo: Pero no termine como tú.
Mifuyu: Eso es porque somos fundamentalmente diferentes.
Mifuyu: Quería ser salvada, a toda costa.
Mifuyu: Está en mi naturaleza...
Mifuyu: Creo que tú más que nadie deberías entenderlo,@siendo mi mejor amiga y todo eso....
Yachiyo: ...
