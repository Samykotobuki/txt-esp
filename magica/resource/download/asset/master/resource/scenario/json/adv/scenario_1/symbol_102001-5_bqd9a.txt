Emiri: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4056_landing_jump2]¡Hyaaagh!
Emiri: Qué asco. Estos familiares están en una@dimensión totalmente diferente a los normales.
Hinano: Oye, pero tú te mantienes a la altura de@ellos muy bien. Lo estás haciendo muy bien.
Emiri: ¡Eso significa mucho viniendo de ti,@Myako-senpai!
Emiri: Realmente no lo entiendo, pero estos Familiares@siguen avanzando en línea recta.
Emiri: Es súper fácil derribarlos,@¿Verdad?
Hinano: Puede que avancen sin tener en cuenta@su seguridad, como una bala de una pistola.
Hinano: ¿Cómo van las cosas por allá?
Nanaka: Hemos sobrevivido a la primera oleada de ataques.
Meiyui: Como dijeron Emily y tú, nuestros enemigos@parecen bastante temerarios.
Meiyui: Si mantenemos la calma y no dejamos que nos@superen, tendremos una buena oportunidad.
Asuka: En otras palabras...
Asuka: ¡Si protegemos esta zona de la bahía, será como@una puerta de hierro para el Fuerte Kamihama!
Asuka: ¡Así podremos luchar contra Walpurgisnacht@en igualdad de condiciones!
Meiyui: Optimista como siempre, ya veo.
Meiyui: La única razón por la que somos capaces de luchar@contra ellos aquí es porque estamos en grupo.
Nanaka: Efectivamente. La zona de la bahía es bastante@extensa...
Nanaka: No tendremos más remedio que dividir nuestras@fuerzas si aparecen enemigos en otros lugares.
Asuka: Ngh, eso es... un buen punto...
Emiri: ¡¿...?!
Emiri: ¡Ah, cielos, chicas, miren eso!
Hinano: ¡Familiares! Parece que vienen de@donde está Eve.
Hinano: Se están dispersando por todo Kamihama...
Nanaka: ¡Todas, en guardia!@¡Otra oleada se acerca!
Nanaka: ¿Qué?
Nanaka: ¿Están apareciendo en otros lugares...@no solo aquí?
Meiyui: Esto se está convirtiendo rápidamente en@una pesadilla.
Kako: ¡Nanaka!
Kako: ¡Los familiares de Walpurgisnacht están@apareciendo por toda la ciudad!
Nanaka: Sí, estoy al tanto.
Asuka: Es casi como si estuvieran respondiendo a los@Familiares de Eve esparcidos por todas partes.
Asuka: Realmente estaba siendo demasiado optimista...
Asuka: ¡Es solo cuestión de tiempo que la ciudad esté@completamente invadida de Familiares!
Emiri: ¡¿Qué HACEMOS, Miyako-senpai?!
Hinano: ...
Nanaka: Eres la Chica Mágica con más experiencia@entre nosotras, Hinano.
Nanaka: No tenemos tiempo para debatir.@Dinos qué hacer, y lo haremos.
Meiyui: No tengo ninguna objeción a eso.
Hinano: Una opción es quedarnos aquí y esperar a@que Walpurgisnacht venga a nosotras.
Hinano: Pero sinceramente, si los Familiares siguen@viniendo, serán una amenaza igualmente.
Hinano: Y en el peor de los casos, podrían incluso@convertirse en brujas...
Nanaka: Entonces...
Hinano: A tiempos desesperados, medidas desesperadas.
Hinano: ¡Todas, divídanse en equipos y sepárense!
Hinano: ¡Nos enfrentamos a los Familiares!
Hinano: Si las Chicas Mágicas cerca de Eve pueden eliminarla,@eso nos librará de la mitad de los Familiares.
Hinano: ¡Hagan lo posible por aguantar hasta entonces!
Mitama: ¿Qué demonios ha pasado aquí...?
Mitama: Estoy segura de que Fendt Hope solía estar@justo ahí.
Mitama: ¿Significa eso que esa cosa masiva que se@encuentra en la distancia es Eve...?
Tsukasa: ¡Mitama! ¿Puedes oírme?
Mitama: ¿Tsukasa?
Mitama: ¿Estás cerca? ¿Están Momoko y las demás@contigo?
Tsukasa: No, nos separamos.
Tsukasa: Estamos usando nuestro poder especial para@comunicarnos telepáticamente contigo.
Mitama: Ya veo.
Mitama: ¿Sabes lo que está ocurriendo en este momento?
Mitama: Parece que las ataduras de Eve fueron cortadas.
Tsukasa: ¡¿Qué?!
Tsukasa: ¡Todavía se mantenían fuertes cuando nos fuimos!
Tsukasa: Aunque hubo un gran terremoto...
Tsukasa: Tal vez Eve se liberó entonces.
Mitama: Ya veo...
Tsukuyo: Además, tenemos asuntos más importantes que@atender.
Mitama: La situación ya es bastante mala...
Tsukuyo: Sí, pero esto es otro asunto totalmente distinto.
Tsukuyo: Mifuyu se hizo una ilusión a sí misma...
Tsukuyo: ...para poder destruir a Fendt Hope.
Mitama: ¡¿...?!
Mitama: ¿Ella hizo qué? ¡¿Utilizó su magia prohibida?!
Tsukuyo: Sí... Pero aún respira.
Mitama: ¿Dónde está ahora?
Tsukuyo: Estamos a punto de llegar a su tienda.
Mitama: Muy bien.
Mitama: Enseguida estoy contigo. Solo hay una@cosa de la que tengo que ocuparme primero.
Mitama: Espérame dentro de la tienda.
Mitama: Mifuyu...
Mitama: ...
Mitama: *Suspira*
Mitama: Primero, tengo que hacer saber a Iroha@y a las demás lo que pasó.
Mifuyu: ...
Tsukasa: *Jadear* *Jadear*
Tsukasa: Por favor, aguanta, Mifuyu...
Tsukuyo: ...
Tsukuyo: ¿No hay nada que podamos hacer para ayudarla?
Tsukuyo: Después de todo, somos Plumas.@Debe haber algo que podamos hacer.
Tsukuyo: Sé que estaban ocultando la verdad, pero no@puedo creer que esto fuera lo que nos esperaba.
Tsukuyo: Me siento tan impotente...
