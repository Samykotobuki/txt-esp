Sana: ¡...!
Sana: [flashEffect:flashWhite2][surround:1004A01][bgEffect:shakeLarge]¡-entonces, Lo siento!
Pluma Negra: [flashEffect:flashRed2][surround:1004A06][bgEffect:shakeSmall][chara:715005:effect_shake]Ugh...
Pluma Negra: (No pensé que ella robaría mi túnica.)
Pluma Blanca: (Le presté uno a una Pluma Blanca, pero@esto es una emergencia, así que está bien.) 
Pluma Blanca: (Tengo que darme prisa para capturar a@Iroha Tamaki)
Pluma Blanca: ...
Touka: ¡Extendamos el milagro de Kamihama a través@de todo el mundo!
Touka: ¡Y nos aseguraremos de que se mantenga@perpetuamente!
Plumas Negras: ¡SII!
Nemu: Queremos que maten a todas las Chicas Mágicas@de Kamihama, sin piedad.
Nemu: Se interponen en nuestro camino al futuro.
Sana: ¿Aún confías en las Magius?
Sana: ¿Incluso cuando llevaron a todas las Plumas@a un [textBlue:Rumor]?
Pluma Blanca: (No importa...No soy como a las que@enviaron fuera.)
Pluma Blanca: (Mis convicciones no pueden cambiar por algo@pequeño como si me usaron como una herramienta.)
Pluma Blanca: (Es más, soy yo la que usa a las Magius@para asegurar la felicidad de todas.)
Pluma Blanca: *Jadea* *Jadea* ¡Por favor, ayuda!
Pluma Blanca: [chara:715103:effect_emotion_surprise_0][se:7226_shock]¡¿Huh?! ¿Q-qué está pasando?
Pluma Blanca: ¡Yachiyo Nanami y su equipo entraron!
Pluma Blanca: ¡¿Qué?! ¿Ya están aquí?
Yachiyo: Si, estoy justo aquí.
Pluma Blanca: ¡¿...?!
Kanagi: Dos plumas...
Kanagi: No debe ser muy difícil de tratar.
Pluma Blanca: ...
Rena: ¡Hey, cuidado con lo que dices! ¡O puede que@no nos escuche!
Kanagi: Solo lo estoy exponiendo, por si acaso. Es@importante que entiendan la situación.
Yachiyo: Escucha. De verdad no queremos pelear con@ustedes
Yachiyo: Por favor, escuchen lo que tengo que decir.
Pluma Blanca: ...
Pluma Blanca: No hay nada de lo que tengamos que hablar.
Pluma Blanca: ¡Tú! ¡Avísale a las otras Plumas!
Pluma Blanca: ¡Las mantendré aquí lo más que pueda@físicamente!
Pluma Negra: Pero no puedo dejarte...
Pluma Blanca: ¡DEPRISA!
Pluma Negra: ¡O-okay!
Kanagi: Esto tomará un rato.
Yachiyo: Si.