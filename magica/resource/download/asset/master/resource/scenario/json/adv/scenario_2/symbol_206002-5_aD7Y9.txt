Madoka: *Risita*
Homura: ¿Madoka?
Madoka: Sólo pensaba que también me alegro de@haberme convertido en una Chica Mágica.
Homura: ¿Qué...?
Madoka: Pude conocer a todas de esta manera.
Madoka: Y sobre todo, ¡tengo fe!
Madoka: En el futuro, en nuestras esperanzas...@Creo en ellas.
Madoka: Y eso es porque me hice amiga de personas@tan maravillosas.
Mami: *Risilla* Estoy empezando a avergonzarme.
Sayaka: Jajaja, sí. Yo también lo creo totalmente.
Sayaka: ¡En el futuro Y nuestras esperanzas!
Kyoko: *Risita* 
Homura: ...
Homura: (Esa vez...)
Homura: ¡Tengo que decírselo a todas!
Homura: ¡Todas estamos siendo engañadas por Kyubey!
Homura: Oh... pero...
Homura: Incluso si intentara explicarlo...
Homura: ... podrían no creerme.
Homura: ......
Homura: (No pensé que me creerían.)
Homura: (No creí que pudieran aceptar nuestro destino.)
Homura: (Pero...)
Homura: (Pero aún sabiendo que nuestro destino era@convertirnos en Brujas, lo hicieron.)
Homura: (Y están tratando de superarlo.)
Homura: (Madoka aún no ha renunciado al futuro,@ni a la esperanza.)
Homura: (Aunque no podamos hacerlo solas,@podemos apoyarnos mutuamente.)
Homura: (En ese caso, tal vez esta línea de tiempo...)
Kyoko: ¿Eh?
Sayaka: Whoa, ¿qué pasa?
Mami: Tengo un pañuelo si lo necesitas.
Madoka: ¡¿Ah, Homura?! ¿He dicho algo malo?
Homura: No... No es eso...
Homura: Sólo estoy muy feliz de haber podido@llegar hasta aquí.
Homura: Aquí es donde debería estar,@el lugar donde nos protegeremos mutuamente,@pase lo que pase.
Homura: Quiero vivir en este mundo.@Junto con Madoka y las demás.
Madoka: *Risilla* ¡Esto es realmente bueno!
Homura: ¡Sí! Me alegro que todas hayamos podido venir.
Sayaka: ¿Los panqueques siempre fueron tan buenos?
Kyoko: ¡Podría comer una docena más!
Mami: Sé que están buenos,@pero ten algo de moderación, ¿de acuerdo?
Kyoko: Oye, soy libre de hacer lo que quiera.
Mami: Cielos... No podrás moverte si sigues así.
Madoka: Oh, cierto.
Madoka: Habrá una reunión en Kamihama para hablar@de todo lo que hizo Magius.
Madoka: Tú también vendrás, ¿verdad, Kyoko?
Kyoko: Nop.
Madoka: ¡¿Qué?!
Sayaka: Nop, tú vendrás.@Fuiste una de las personas involucradas.
Mami: Así es. Deberías enterarte de lo que pasó.
Kyoko: Bien, sólo denme un resumen cuando termine.@Entonces lo sabré.
Madoka: Creo que no estás entendiendo el punto...
Kyoko: ¿Podemos centrarnos en lo que es importante@ahora? ¡Más panqueques por aquí!
Mami: Supongo que tendremos que recogerla@de camino a la reunión.
Sayaka: Parece que sí...
Homura: ...
Homura: (Nunca pensé que llegaríamos a pasar@nuestros días así...)
Homura: La lucha continúa.
Homura: Las Brujas no han desaparecido@y no se han resuelto todos los problemas.
Homura: Pero estaremos bien.
Homura: No importa lo que pase desde ahora,@podremos soportarlo. Sé que podremos.
