...
Momoko: ¡Rena! ¡Kaede! ¡Esta es la última!
Kaede: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4004_equip_arms_magic_vh]¡Fyaaah!
[flashEffect:flashRed2][se:6151_hit_magic_01_vh][bgEffect:shakeLarge][name:sprite_0:effect_shake]¡¿...?!
Kaede: ¡La tengo inmovilizada!
Rena: ¡Bien hecho, Kaede!
Rena: ¡Momoko!
Momoko: ¡Voy a terminar con esto de un solo golpe!
Rena: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4052_landing_dive_sv]¡Jaaaaaaaaah!
Momoko: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:6101_hit_slash_01_vh]¡Rrraaaaaaaaagh!
[se:6259_hit_spike_03_v][se:6100_hit_slash_01_s][flashEffect:flashRed2][bgEffect:shakeLarge][name:sprite_0:effect_shake]¡¿...?! ¡¿¡¿¡¿...?!?!?!
Rena: Fiuu. Estoy agotada.
Momoko: ¡Pero al menos ahora podemos disfrutar la vista de@este enorme jardín!
Kaede: ¡Sí!
Kaede: Ahora todo lo que tenemos que hacer esperar a que Yachiyo@y las otras estén en posición.
Kyoko: ¿No crees que se siente demasiado facíl?
Kaede: ¿Crees que algo más viene?
Kyoko: Entendirían a lo que me refiero si hubieran pasado tiempo@cerca de las tales Magius.
Kyoko: Ellas no tienden trampas a medias como@esta, no es su estilo.
Kyoko: Se los dije.
Kaede: Aún así, ¡sólo es una!
Momoko: ¡Sí! Nos podemos encargar de una, pan comido.
Momoko: ...O no.
怒是王死 劔與惑即 日幻賊術
Tsukasa: Mifuyu, ¡¿no es esta...
Tsukuyo: Una de las Brujas mascota de Alina?!
Mifuyu: En efecto. Y no es sólo cualquier@Bruja de su colección.
Mifuyu: Esta es su angel precioso, la Bruja a la que le daba de comer@otras Brujas para que pudiera protegerlas.
Tsukasa: ¡Este no es un angel precioso! ¡Esto es un@mounstruo demoniaco!
Tsukuyo: ¿E-ella creó tal atrocidad?
Mifuyu: Sólo Magius y yo sabíamos de su@existencia.
Tsukasa: Estoy con los pelitos de punta.
Kyoko: Odio decirlo, pero se los dije. Desearía@estar mal.
Kyoko: ¿Tienen algo más después de esto?
Mifuyu: No puedo garantizar que no vengan más.
Kyoko: Entonces parece que tenemos que seguir en pie@un poco más.
Kyoko: Pero, deberían llegar a la capilla en cualquier momento, ¿no?
