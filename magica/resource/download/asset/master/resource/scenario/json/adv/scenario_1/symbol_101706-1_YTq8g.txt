Iroha: Supongo que las Plumas Negras y Blancas realmente no saben nada.
Iroha: Pero también estaba esa Pluma Blanca que nos topamos@cuando empezamos con todo esto...
Yachiyo: Si...
Yachiyo: Quizás no han compartido la informacion@que estamos buscando con todas.
Yachiyo: ¿O quizás las que saben sobre eso no están a cargo de proteger a los [textBlue:Uwasa]?
Iroha: Si eso es lo que es...
Iroha: Tendría sentido. Hasta ahora sólo hemos encontrado@Plumas que estaban protegiendo [textBlue:Rumores].
Iroha: Así que no hay razón para que sepan.
Iroha: (Tampoco esas personas...)
"Rumor del Candado del Rompecabezas de Azulejos"
Las tapas de pozos de acceso de la ciudad tienen patrones@que, cuando son alineados con la ruta, muestran un@camino a una caja fuerte en la Municipalidad de Kamihama.
Se dice que ese camino está lleno de trampas.
Pluma Blanca: ¡Han estado causando bastantes estragos@por estos lados, pero sus ataques terminan ahora!
Pluma Blanca: ¡Adelante!
Pluma Negra: ¡En nombre de la libertad!
Iroha: Tenemos que lograr que nos digan esta vez...
Yachiyo: Si. ¡No vamos a permitir que se escapen!
"El Rumor del Monstruo de la Moda"
Las chicas vestidas con confianza con su mejor traje@podrian encontrarse con esta criatura.
Te arrastra a un callejón, y si no admites@de que se ve mejor que tú, ¡te desnuda!
Pluma Negra: Ya te dije, ¡no sé nada!
Pluma Blanca: No tiene sentido estar aquí ahora que el [textBlue:Rumor] fue borrado.
Pluma Blanca: Retiremonos.
Pluma Negra: Si...
Iroha: ¡Espera!
Iroha: Lo sabía... Nadie sabe sobre ellas.
Iroha: Y todas reaccionaron de la misma forma, así que no creo que estén mintiendo.
Iroha: Quizas deberiamos cazar a una Pluma que no@estee protegiendo [textBlue:Rumores].
Yachiyo: O encontrar a alguien que esté a cargo. Podrian@saber más.
Iroha: Cierto.
Iroha: Yachiyo, podríamos repasar lo que@aprendimos una vez más?
Yachiyo: Seguro. No vamos a llegar a ningún lado corriendo en círculos de esta forma.
Yachiyo: Podemos empezar por anotar todos los lugares en donde destruimos [textBlue:Rumores] en este mapa.
Iroha: Está bien.
Iroha: Aunque se siente como que hay algo@importante que estamos olvidando...
(Player Choice): ¡Mokyu!@(¡Ese cuaderno!)
(Player Choice): ¡Mokyuu!@(¡Los atributos especiales de las Alas de Magius!)
userName: ¡Mokyu!
Iroha: ¡Oh!
userName: ¡Mokyuu!
Iroha: No, no es eso. Es como que ya tenemos@una gran fuente de información...
Iroha: ¡Oh!
Yachiyo: ¿Qué pasa?
Iroha: ¿Por qué no marcamos todos los [textBlue:Rumores] en este@mapa?
Iroha: Tenemos anotados todos los que destruimos@hasta ahora...
Iroha: Y tenemos algunos más de hoy.
Iroha: Nuestro objetivo es descubrir dónde está la base de las Alas de@Magius, ¿no es así?
Iroha: Así que deberíamos usar toda la información que ya@tenemos! ¡Tenemos mucha!
Yachiyo: ¿Mucha? Oh, te refieres al Archivo de Rumores de Kamihama.
Iroha: ¡Si! ¿Qué te parece? ¡¿Podemos intentarlo?!
Iroha: ¡Podemos marcar todos los artículos que tienen una@ubicacion anotada!
Yachiyo: Okey. ¡Vamos a marcarlos a todos en el mapa!
Yachiyo: Muy bien. Deberíamos empezar por el índice.
Iroha: ¿Hay un índice?
Yachiyo: Si, atrás de todo.
Yachiyo: [textBlue:Rumores] sin restricciones... [textBlue:Rumores]@que aparecen bajo circunstancias especiales...
Yachiyo: [textBlue:Rumores] que aparecen en ciertos lugares...@Los dividi a todos en categorías.
Iroha: ¡Wow, eres tan dedicada!
Yachiyo: Vamos a empezar a marcarlos.
Iroha: Okey. Tengo una lapicera lista.
Rumores Sin Restricciones
Estos Rumores aparecen sin ninguna clase de explicación. Solo unos pocos están limitados por ubicación.@Ejemplo: Agua de la Miseria.
Rumores Que Aparecen Bajo Circunstancias Especiales
Estos Rumores aparecen debido a las acciones de alguien.@Solo unos pocos están limitados por ubicación.@Ejemplo: La Regla de la Ruptura de Amistades.
Rumos Que Aparecen En Ciertos Lugares
Estos Rumores siempre están en una ubicación fija.@Ciertan condiciones podrían requerirse para entrar.@Ejemplo: Santuario de Reunión
Iroha: Los marcamos a todos en el mapa.
Iroha: Parece que no hemos investigado tanto al este@de Chuo Ward.
Yachiyo: Si. Tenemos un acuerdo por ahí.
Yachiyo: No se nos permite ir al este de@Chuo Ward sin una buena razón.
Yachiyo: Cuando no había tantas Brujas, hubo muchas@disputas territoriales.
Yachiyo: Así que elaboramos un acuerdo para dejar de luchar@entre nosotras por ellas.
Iroha: Ya veo...
Iroha: De todas formas, los marcamos a todos en el mapa, @pero...
Yachiyo: ¿Pero?
Iroha: Yachiyo...
Yachiyo: Iroha....
Yachiyo: Oh dios mío... Nunca hubiera esperado@esto.
