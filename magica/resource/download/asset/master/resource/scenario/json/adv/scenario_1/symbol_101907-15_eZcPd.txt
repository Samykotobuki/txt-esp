[se:7213_witch_noise_01]｜¡¡0｡*ｰ(●(工)●)ｰ*｡0!!｜
Mifuyu: ...
Momoko: ¡Hombre, esta cosa simplemente no cede a menos@de que tengamos algo especial bajo la manga!
Mifuyu: Puede que tenga uno de esos.
Momoko: ¡¿Qué?! ¡¿A que te refieres, Mifuyu?!
Momoko: ¿Por qué no dijiste nada antes?
Tsukasa: ¿Un plan secreto? Mifuyu, ¡¿No querras decir?!
Tsukuyo: ¡No debes! ¡Está prohibido!
Momoko: ¿De qué están hablando?
Tsukuyo: Estás planeando poner una ilusión sobre@tí misma, ¡¿no es así?!
Mifuyu: Sí, exacto.
Momoko: ¿Podrías explicar?
Mifuyu: No importa si tenemos un montón de magia@y los hechizos mas fuertes.
Mifuyu: Las Chicas Mágicas sólo puden liberar cierta cantidad@de magia en un solo ataque.
Momoko: Sí, eso eso cierto.
Mifuyu: Si pongo un illusión sobre mi misma, entonces podré@liberar hasta el último gramo de magia de una vez.
Mifuyu: Me convenceré de que puedo usar mucha más@magia de la que normalmente puedo.
Momoko: ¡No!
Tsukuyo: ¡Es demasiado peligroso!
Tsukasa: ¡Si haces eso, Mifuyu, tu Sould Gem no@sobrevivirá!
Mifuyu: Tengo que intentarlo.
Momoko: ¡Sí, no me agrada esto!
[flashEffect:flashWhite2][bgEffect:shakeLarge][se:7213_witch_noise_01]｜.ﾟ○*;.(＾(工)＾).;*○ﾟ.｜
Momoko: Ugh...
Mifuyu: ¡Esto no está a debatge!
Mifuyu: No puedo dejar que ustedes mueran.
Mifuyu: Por favor, déjenme tomar la responsabilidad.
Momoko: No...
Mifuyu: Por favor.
Momoko: No me pidas permiso, Mifuyu...
Mifuyu: ...
Mifuyu: Entonces tomaré mi propia decisión.
Mifuyu: ¡Tsukuyo! ¡Tsukasa!
Tsukuyo & Tsukasa: ¡¿S-sí?!
Mifuyu: Cuando esto acabe, por favor recuperen mi cuerpo@y llévenlo al lugar de la Coordinadora.
Mifuyu: Momoko...
Mifuyu: Por favor mantén esto en secreto de Yachan.
Mifuyu: Por lo menos hasta que todo esto acabe...
Momoko: ¡Oye, espera! ¡¡¡Ni siquiera PIENSES en de verdad@hacerlo!!!
Momoko: ¡¿Qué carajo se supone que le diga?!@¡Joder, Mifuyu!
