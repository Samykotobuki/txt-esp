Sayaka: ¿Por dónde deberíamos empezar?
Homura: Necesitamos alguna pista para@encontrar a Mami.
Madoka: Sí, y también tenemos que ver si Iroha y@las demás están bien.
Madoka: [flashEffect:flashRed2][surround:715000A03][chara:200101:effect_shake][bgEffect:shakeLarge]¡¿Aaah?!
Homura: ¡Madoka!
Madoka: ¡E-estoy bien!
Sayaka: ¡¿Oye, por qué fue eso?!
Pluma Negra: Vrrugh...
Sayaka: ¿Son las lacayas de Magius?
Pluma Negra: Raaagh... Aaagh...
Sayaka: Antes no actuaban así, ¿no?
Madoka: No, algo está mal. ¿Por qué nos atacarían en@la calle, al aire libre?
Pluma Negra: [flashEffect:flashWhite2][surround:715010A01]¡Vrrraaagh!
Homura: [flashEffect:flashRed2][surround:715010A02][chara:200301:effect_shake][bgEffect:shakeLarge]¡Agh!
Homura: No solo eso. ¡Son más fuertes ahora!
Homura: ¡Estamos en problemas a menos que nos@transformemos y luchemos!
Madoka: ¡Entendido!
Madoka: Atraigámoslas a algún lugar fuera del camino.@Alguien podría salir herido si luchamos aquí.
Sayaka: ¡De acuerdo! ¡Las cubriré por detrás!
