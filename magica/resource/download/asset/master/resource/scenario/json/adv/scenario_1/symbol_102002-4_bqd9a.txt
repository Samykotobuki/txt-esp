Shizuku: *Jadea* *Jadea*
Mitama: Muchas gracias, Shizuku.@Tu Fusión Espacial realmente es muy útil.
Mitama: Espero que no te haya supuesto un@gran esfuerzo.
Shizuku: Bueno, antes purificaste mi Soul Gem@por mí...
Shizuku: No creo que pueda conseguir salir de@la tienda y luchar todavía...
Shizuku: Así que me alegro de poder ayudar de@esta manera.
Mitama: Sí, tu ayuda fue inestimable.
Tsukasa: Um, ahora que terminaron,@¿podrías mirar a Mifuyu...?
Tsukuyo: Está dormida en el catre de aquí.
Mifuyu: ...
Mitama: ...
Tsukuyo: ¿Qué te parece...?
Tsukasa: Sí, ¿puedes ayudarla? ¿Cómo se ve ella?
Mitama: No creo que esté muerta.
Tsukasa: ¡Entonces...!
Mitama: Sin embargo, tampoco puedo asegurar que@esté viva.
Mitama: No sé exactamente cómo decirlo, pero parece@estar en una especie de coma.
Tsukuyo: ¿Hay alguna forma de ayudarla?
Mitama: No estoy segura...
Mitama: Su Soul Gem parece que podría romperse si alguien@la tocara con demasiada brusquedad.
Mitama: No creo que mis poderes puedan hacer nada por ella.
Tsukuyo: No...
Tsukuyo: ...
Tsukuyo: Mifuyu...
Tsukasa: *Llorar*
Mitama: Sin embargo, hay una cosa que podemos hacer.
Tsukuyo: *Llorar*
Tsukuyo: Sí, ya sé lo que es.
Tsukasa: Quieres decir que no debemos dejar que su@sacrificio sea en vano, ¿no?
Tsukuyo: Cierto.
Mitama: Sí.
Mitama: Tenemos que estar seguras de que Mifuyu pueda@mantener la cabeza alta...
Mitama: ...cuando se despierte de su sueño.
Mitama: ¡¿...?!
Mitama: ¡¿Qué demonios?!
¡Hobwheh hwih!
¡Hbwet'sh mbhke nbhst!
Tsukuyo: ¡Están provocando un tornado...!@¡Parece que están reuniendo algo!
Tsukasa: ¡Casi parece que están construyendo un nido!
Mitama: ¡Incluso están reuniendo a los familiares de@Walpurgisnacht!
Mitama: Espera... La razón por la que los Familiares@de Eve están construyendo un nido es...
Mitama: ¡¿Para poder acumular las impurezas que lleva@Walpurgisnacht?!
