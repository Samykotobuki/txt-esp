Iroha: Estoy buscando en el [textRed:Santuario Séance].
Iroha: Pensé que coleccionar todos los sellos@podría llevarme a algo.
Yachiyo: Ya veo...
Iroha: ¿Qué?
Yachiyo: Parece que estás investigando@mucho más seriamente de lo que esperaba.
Yachiyo: De todos los rumores,@¿Por qué el [textRed:Santuario Séance]?
Iroha: ¿Por qué? ¿Es uno de los rumores que está@en tus Archivos de Rumores de Kamihama?
Yachiyo: No tengo que responder a esa pregunta.
Iroha: Aww...[chara:100102:effect_emotion_sad_0][se:7224_fall_into]
Yachiyo: Lo más importante es que deberías@mantenerte alejada de todos los Rumores.
Yachiyo: Como te dije antes...
Yachiyo: Meter las narices en las cosas@como esta solo terminará en sufrimiento.
Iroha: Lo sé, pero...
Yachiyo: Vas a seguir investigando sin@importar lo que diga, ¿No es así?
Iroha: Sí.@Esto podría llevarme a Ui.
Yachiyo: Cuando se trata de tu hermana,@eres muy terca, ¿Lo sabías?
Iroha: Um, Yachiyo...
Yachiyo: ¿Qué pasa?
Iroha: ¿Por qué participas en este evento?
Yachiyo: Tampoco tengo que responder a esa pregunta.
Iroha: Eso también, eh...
Iroha: Pero tienes la intención de ver esto@hasta el final, ¿verdad?
Yachiyo: Sí, ese es el plan.
Iroha: En ese caso, bueno... ¡Tengo A!
Yachiyo: ¿Qué es lo que te pasa?
Yachiyo: Ah, ya veo...
Yachiyo: Tengo...
Yachiyo: Qué suerte, B.
Iroha: ¡Bien entonces!
Yachiyo: *Suspira* Supongo que no hay opción.@Juntémoslas.
Iroha: ...
Yachiyo: ...
Yachiyo: Hmm...@Qué lugar tan ordinario.
Iroha: ¿Sabes dónde está?
Yachiyo: Sí, sígueme.@También podría mostrarte el camino.
Iroha: ¡Gracias!
Yachiyo: ¿Eh?@[chara:100201:effect_emotion_surprise_0][se:7226_shock]Oye, Iroha.
Yachiyo: Se te ha caído algo.
Iroha: ¿Eh?
Yachiyo: Este cuaderno es tuyo, ¿No?
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿Qué?! Esto es...
Yachiyo: ¿Por qué te asustas?
Iroha: No me asusté, es solo@mi tarea, ¡Eso es todo!
Yachiyo: [wait:1.0][chara:100201:lipSynch_1][chara:100201:voiceFull_fullvoice/section_101303/vo_full_101303-4-44]De acuerdo...
