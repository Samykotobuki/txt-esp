Felicia: Hmmm-hmmm-hmmm♪@[chara:100501:effect_emotion_joy_0][se:7222_happy]Hmmm-hmmm-hmmmmmm♪
Iroha: ...
Iroha: Oye, Felicia...
Felicia: ¿Qué pasa?
Iroha: ¿Vas a seguir trabajando de mercenaria?
Felicia: Bueno, umm... Quiero decir,@¿Supongo que tengo que hacerlo?
Felicia: Apuñale a esas Alas de No-sé-qué por la espealda,@asi que no es como si tuviera elección.
Iroha: U-um, bueno... Felicia...
Iroha: Esto es solo mi opinión, pero...@Creo que tal vez deberías dejarlo...
Iroha: Hoy lograste contenerte,@pero puede que vuelvas a enloquecer.
Iroha: Cuando haces eso no notas lo que pasa@a tu alrededor, y podrías herir a alguien.
Iroha: Además, si sigues cambiando de bando@según la recompensa que te dé la gente...
Iroha: Con el tiempo, todos los que te rodean...
Iroha: ...se volverán tus enemigos, Felicia.
Felicia: ¿Y qué?
Iroha: ¿Huh?
Felicia: ¿Qué puedo hacer sobre eso?
Felicia: Sí, tal vez me vuelvo loca cuando hay una Bruja,@pero básicamente esa es mi especialidad.
Felicia: Si quiero sobrevivir, no hay nada más@que yo pueda hacer excepto cosas de mercenaria...
Felicia: Me alegra que me vayas a hacer la cena...
Felicia: Pero quisiera que no digas mierda como esa...
Iroha: Pero...
Felicia: ¿Qué [chara:100501:effect_emotion_angry_1][se:7218_anger]quieres que haga?
Felicia: ¡Sé que no puedo seguir haciendo esto@por siempre!
Felicia: Sabes, incluso cuando trato de no@traicionar a nadie, pasa de todas formas.
Felicia: Como, ayudando a alguien que está de malas@con alguien que ayude antes.
Felicia: ¡Antes de que me dé cuenta, todos actúan@como si los hubiera doble-traicionado o algo así!
Felicia: ¿Qué se supone que haga con eso?
Iroha: Bueno, entonces, tal vez...
Iroha: ¿Qué tal si te unes a mi equipo?
Felicia: ¿Ah?
Felicia: ¿Hablas en serio...?
Iroha: Sí. Luchemos juntas como amigas,@y comamos juntas también.
Felicia: ¿Lo dices de verdad?
Iroha: De verdad.
Felicia: Yo... voy a tener que pasar, aún así...
Iroha: ¿Q-qué? [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿Por qué?!
Felicia: Me encantaría poder vivir sin traicionar a@nadie, pero no puedo pagarte la comida...
Yachiyo: Entonces ven a quedarte conmigo.
Iroha: ¿Yachiyo?
Yachiyo: Puedes ser mi mercenaria personal@mientras vivas bajo mi techo.
Yachiyo: Mi casa solía ser una pensión,@así que hay muchas habitaciones.
Felicia: [chara:100501:effect_emotion_surprise_0][se:7226_shock]¡¿De verdad?!
Yachiyo: Tus servicios se considerarán@el pago de tu alquiler.
Yachiyo: Si te parece bien, ven a quedarte.
Felicia: ¡Muchas gracias, [chara:100501:effect_emotion_joy_0][se:7222_happy]Yachiyo!
Iroha: Gracias, Yachiyo.
Yachiyo: Se me antojo recién, eso es todo.
Tsuruno: Je je... Acabo de tener una gran idea.
Yachiyo: ¿Qué? ¿Un nuevo plato para tu restaurante?
Tsuruno: ¡NOP!
Tsuruno: Si no tienes dinero, ¿por qué no@vienes a trabajar a Banbanzai?
Tsuruno: Puede que no sea fácil porque@no tienes experiencia.
Tsuruno: Pero debería estar todo bien,@sobre todo si sólo estás ayudando.
Felicia: ¿Por qué? No voy a trabajar gratis.@Eso es para idiotas.
Tsuruno: ¡No trabajarías gratis!
Tsuruno: No es mucho, pero te pagarían por hora.
Tsuruno: No tendriás que pagar por comer en lo de Yachiyo,@y además tendrias el almuerzo del personal!
Felicia: ¡¿Comida gratis?![chara:100501:effect_emotion_surprise_0][se:7226_shock]
Felicia: Espera, ¿qué es el almuerzo del personal?
Tsuruno: Cuando terminas tu turno, puedes@comer algo del restaurante.
Felicia: ¡Cena!
Tsuruno: Además empezamos a hacer entregas,@pero todavía no nos alcanza el personal.
Tsuruno: Mi papá y yo estuvimos hablando de@contratar gente que se encargue de eso.
Felicia: ¿En seeeeerio?
Tsuruno: ¡Sip!
Felicia: ¡Qué bieeen! ¡Muchas gracias, Tsuruno!
Tsuruno: ¡Puedes llamarme Tsuruno Onee-san,@si quieres!
Felicia: ¡Tsuruno!
Tsuruno: Solo Tsuruno entonces...[chara:100301:effect_emotion_sad_0][se:7224_fall_into]
Iroha: *Risita* Me alegro por ti, Felicia.
