Yachiyo: Iroha... Estoy rompiendo el equipo...
Iroha: ...
Iroha: ¿Eh... qué? ¿Estás rompiendo el equipo?
Yachiyo: Así es.
Iroha: Espera un segundo... Eso es...
(Player Choice): ¡Mokyu!@(Debe tener un plan.)
(Player Choice): ¿Mokyu?@(Pídele que te explique.)
userName: ¡Mokyu!
Iroha: Ah...
userName: ¿Mokyu?
Iroha: (P-pero... no hay que confundir lo que ella@acaba de decir...)
Iroha: Ah...
Iroha: (Así es, las dos no fuimos influenciadas@por la memoria de Mifuyu).
Iroha: (Debe estar actuando para que no tengamos@que luchar contra los Rumores...)
Iroha: (Como debo responder... ya le dije@que me negaba...)
Iroha: ...
Iroha: (Al menos por el momento...)
Iroha: S-sí, deberíamos romper el equipo.
¡Yachiyo: ...?!
¡Yachiyo: ...me alegro de que lo entiendas.
Iroha: Por supuesto. He visto el mismo recuerdo que tú.
¡Yachiyo: Muy bien, entonces. Me voy a ir de aquí ahora.
Iroha: Ah, en realidad yo también...@ 
¡Yachiyo: ...
Iroha: Me pregunto si Tsuruno y las demás están@afuera ya...
¡Yachiyo: Estoy segura de que lo están.
¡Yachiyo: Si la memoria de Mifuyu las afectó, entonces es@probable que ya se hayan ido.
Iroha: No es que importe, ya que hemos roto el equipo.
¡Yachiyo: ...
Iroha: (Si realmente les han lavado el cerebro a todas...)
Iroha: (Sería mi culpa.)
Iroha: (No puedo creer que estuviera tan ansiosa por creer@que no querían luchar).
Iroha: (Todo esto es porque no escuché cuando@Yachiyo me dijo que no fuera...)
Iroha: ...
[se:7214_witch_noise_02]|KKT|
Iroha: El Rumor...
Iroha: (¿Así que no tenía sentido fingir?)
¡Yachiyo: Vino a detenernos, como yo pensaba...
[se:7214_witch_noise_02]｜0=|⊃―☆｜
¡Yachiyo: ¡Fuera de mi camino!
