Iroha: Qué mal que no pudiésemos comprar el helado...
Iroha: Me pregunto si Tsuruno sigue enfadada...
Yachiyo: Bueno, ella normalmente se olvida de estas@cosas en cuanto se queda dormida.
Yachiyo: Estoy más preocupada por nuestra ladrona de helado.
Yachiyo: Felicia, ¿Terminaste de pelar las cebollas?
Felicia: ¿Ngh?
Yachiyo: [chara:100201:effect_emotion_surprise_0][se:7226_shock]¡Espera un segundo! ¡¿Qué haces leyendo@manga?!
Felicia: ¡Odio tocar cebollas! Me hacen llorar.
Yachiyo: Por eso hay que remojarlas primero en agua. @¿No te lo enseñé ayer?
Felicia: ¿Lo hiciste?
Tsuruno: ¡Estoy aquí!
Iroha: Ah, ahí está Tsuruno...
Yachiyo: Juzgando por su voz, diría que ya@no está enfadada.
Tsuruno: ¡Guau! ¡Estás haciendo la cena!
Iroha: ¡Bienvenida, Tsuruno!
Tsuruno: ¡No las había visto desde el almuerzo!
Tsuruno: Ah, compré esto. Lo dejaré en el@congelador.
Iroha: ¿Es...helado?
Tsuruno: *Risilla*
Tsuruno: Sé que ayer me comporté como un bebé...@¡Por suerte esto lo arreglará!
Tsuruno: Perdón por enfadarme, Felicia.
Felicia: [chara:100501:effect_emotion_surprise_0][se:7226_shock]¡¿Esto es para mi?! ¡¿Puedo comerlo?!
Tsuruno: ¡Claro, podemos comerlo juntas!
Felicia: ¡Yachiyo! ¡¿Puedo tomarlo ahora?!
Yachiyo: Cuando acabes tu comida.
Felicia: [chara:100501:effect_emotion_joy_0][se:7222_happy]¡Genial! ¡Eres la mejor, Tsuruno!
Yachiyo: ¿Quieres ayudarnos a preparar la cena?
Tsuruno: ¡Claro!
Yachiyo: Todo listo. Toma una olla y ponla con agua@a hervir.
Tsuruno: ¡Lo tengo!
Iroha: *Risilla*
Tsuruno: ¿Eh? ¿Qué pasa, Iroha?
Iroha: Estaba pensando... Todas nosotras aquí, esto@se siente como una familia, ¿no creen?
Iroha: ...
Iroha: Habría sido impensable,@incluso hace poco tiempo.
Iroha: Nunca habría pensado que estar con otras@personas se sentiría tan natural, tan divertido...
Iroha: De verdad...
