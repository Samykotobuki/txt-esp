Tsuruno: ... Hola.
Emiri: ¡Tsurupi!@¿Cómo va todo?
Emiri: ¡Cierto, el entrenamiento!@¿Cómo ha ido? ¿Te hiciste más fuerte?
Tsuruno: Ummm... ¿está Akira-sensei aquí hoy?
Emiri: ¿Akira-chi?@Ella debería estar aquí pronto, supongo.
Tsuruno: ... Ya veo.@Empezaré contigo, Emily-sensei...
Tsuruno: Después tengo que disculparme@con Akira-sensei y luego con Meiyui-sensei...
Emiri: ¿Disculparte?@... ¿Por qué?
Tsuruno: Bueno, verás...
Akira: ¡Ah! ¡Tsuruno!@¡Qué buen momento!
Tsuruno: ¡Akira-sensei...!
Akira: ¡Meiyui!@¡Tsuruno está aquí!
Meiyui: ¡Oh!@¡Tsuruno, qué coincidencia!
Tsuruno: ¡Eek!@¡Meiyui-sensei también...!
Emiri: ¡Hola Chunchun!@Ha pasado como una eternidad, ¿no?
Meiyui: Por favor, deja de llamarme así...
Tsuruno: ... S-Sobre lo que pasó.@Bueno, ummm...
Tsuruno: ¡L-Lo siento mucho!
Akira: ¿Eh?@¿Por qué te disculpas?
Tsuruno: Err, bueno, el incidente en la fábrica...@Realmente metí la pata y...
Meiyui: Oh, sobre eso.@Tengo un mensaje del gerente de la fábrica.
Tsuruno: ¿¡Eh!?@[chara:100301:effect_emotion_surprise_0][se:7226_shock][chara:100301:lipSynch_0][freeze:1.0][chara:100301:cheek_1][chara:100301:face_mtn_ex_030.exp.json][chara:100301:mouthOpen_0][chara:100301:tear_1][chara:100301:lipSynch_1]... Lo sabía. Aquí viene...
Meiyui: "Por favor, vuelve cuando quieras."@... Eso es todo.
Tsuruno: ¿Eh?
Meiyui: Todos en la fábrica te adoraron por@lo duro que trabajaste. Estaban encantados.
Meiyui: Dijeron que fue divertido trabajar contigo y@que quieren que vuelvas.
Tsuruno: ... ¿Eh? ¿Qué?
Akira: ¡Y yo tengo un mensaje para ti de parte@de la dueña de ese perro perdido!
Akira: ¡Dijo que muchas gracias por@encontrar a su precioso perrito!
Akira: La dueña es una simpática anciana@que vive sola.
Akira: Así que ese perro es su única familia.
Akira: ¡No sabes lo agradecida que estaba@por lo que hiciste, Tsuruno!
Tsuruno: ¿Eh? ¿Qué? ¿Eh?
Emiri: ¡Así se hace, Tsurupi!
Akira: ¡Gracias, Tsuruno!
Meiyui: Les ayudarás de nuevo, ¿verdad?
Tsuruno: ¡S-Sí!
Tsuruno: ... Y así terminaron las cosas.[chara:100301:effect_emotion_joy_0][se:7222_happy]
Yachiyo: ¡Me alegro por ti, Tsuruno!
Tsuruno: En otras palabras...
Tsuruno: ¡Mi entrenamiento fue todo un éxito!
Yachiyo: ¿Ah, sí?@... No estoy segura de entender, pero...
Yachiyo: En fin, parece que al final todo salió bien, ¿no?
Tsuruno: ¡Sí, así es!@Ahora sí que puedo decir que...
Tsuruno: ¡Me he vuelto MÁS FUERTE!
Yachiyo: ... ¿En serio?@Quieres decir mentalmente, ¿verdad?
Tsuruno: ¡No, en todos los sentidos!@¡Porque fue una buena experiencia! Creo.
Yachiyo: ... No creo que así sea como funcione...
Tsuruno: ¡Maestra Yachiyo!@¡Te mostraré mi nuevo poder!
Yachiyo: No estás escuchando, ¿verdad?@Sí, definitivamente no.
Tsuruno: ¡Vamos, practiquemos!@¡Luchemos un poco![chara:100301:effect_emotion_joy_0][se:7222_happy]
Yachiyo: ...
Yachiyo: Iré a buscar a Momoko y a las demás.
Tsuruno: ¡¡Ugh, Yachiyo se escapó!!@¡Ah, eso es! ¿¡Rena, dónde estás!?
