Chisato: "Con la ayuda de Touka y Nemu,@pudimos ponernos en contacto con Kurumi.@Las tres genios se pusieron a trabajar@en arreglar la situación."
Kurumi: "Así que básicamente, LycoReco@Villa Mikazuki se intercambiaron...@Ahora existen en mundos completamente diferentes...@No lo quiero creer."
Touka: Pero no hay Ciudad de Kamihama en tu mundo,@¿cierto?
Touka: Aparentemente eres una genio, pero solo puedes ver las@a través del velo del conocimiento común.
Touka: Qué decepción.
Kurumi: "Mira, yo me especializo en guerra cibernética.@Soy un tipo de genio completamente@diferente de genio que ustedes dos."
Kurumi: "Pero aun dejando de lado el hecho de que@la Ciudad de Kamihama, donde viven, existe aquí,@hay demasiadas diferencias entre nosotras...@No quiero creer en viajes a otros mundos,@pero no hay otra manera de explicar todo esto..."
Chisato: [chara:406101:effect_emotion_surprise_0][se:7226_shock]¿Eh? ¿Quieres decir que REALMENTE nos hicieron@el isekai?
Takina: ... Has estado diciendo eso desde el principio,@Chisato.
Chisato: Yo simplemente pensé que sería gracioso, pero@ellas lo están diciendo en serio.
Touka: Aun así, hay demasiadas incógnitas@en el universo.
Touka: De igual manera, no podemos decir que estamos@seguras de que eso es lo que está pasando...@Es solo una posibilidad.
Takina: ... ¿Podremos regresar a casa?
Touka: Mmm... ¿Qué piensas, Kurumi?
Kurumi: "Gracias a ese [textBlue:Rumor] o lo que sea,@fuimos capaces de comunicarnos...@Así que si Touka, Nemu y yo trabajamos juntas,@tal vez podamos conseguir algo."
Touka: *Risilla* Como dijo ella, Nemu.
Nemu: Yo pienso lo mismo... @[chara:101471:lipSynch_0][wait:0.8][chara:101471:cheek_0][chara:101471:face_mtn_ex_011.exp3.json][chara:101471:lipSynch_1]*Risilla*
Nemu: Bien entonces, manos a la obra. Para el resto de@ustedes, necesito que me ayuden a investigar algo.
Chisato: [chara:406101:effect_emotion_joy_0][se:7222_happy]¡Wuujuu! ¡Nos toca ser estudiantes normales!
Takina: ... No estamos aquí para jugar.@Estamos conduciendo una investigación.
Chisato: ¡Mmph! No seas tan aburrida, Takinaaa...
Iroha: Aun así, Kurumi realmente es interesante....
Ui: Sí... es una hacker, ¿verdad? Así es como consiguió@inscribirlas a las dos en nuestra escuela.
Nemu: Muy bien, esto es lo que necesito que investiguen.
Nemu: La mayoría de problemas de este tipo tienen@que ver con [textBlue:Rumores].
Nemu: Aunque dudo que algún [textBlue:Rumor] se haya@materializado, no podemos estar seguras de@que no existe ninguno.
Nemu: Por lo tanto, me gustaría que condujeran una@encuesta.
Takina: Una encuesta... Definitivamente podemos hacer@eso. Aunque es sorprendentemente clásica.
Chisato: Ya que no tenemos a DA vigilándonos,@¿por qué no hacemos esto más interesante?
Chisato: ¡Como... ir de incógnito a una escuela!
Takina: Chisato... No es momento para juegos...
Nemu: De hecho, como los [textBlue:Rumores] son el tema aquí,@infiltrarse en una escuela es bastante apropiado.
Takina: ¿Eh?
Touka: Los jóvenes tienden a sentirse atraídos por los@rumores por su naturaleza misma.
Takina: Dicho eso, infiltrarse no será fácil sin la@ayuda de DA...
Kurumi: "¡Oh mira!@La Escuela Afiliada a la Universidad de Kamihama,@la escuela a la que Iroha y algunas otras aquí atienden,@está abierta a aceptar un par de estudiantes@por unos días para asistir a clases."
Nemu: ... ¿Qué diantres hiciste?
Kurumi: "*Risilla* Secreto comercial. Como dije,@soy un tipo de genio diferente a ustedes.@No se preocupen, nadie salió herido.@Y yo limpiaré todo después."
Chisato: ¡Yaaay! ¡Voy a tener la vida escolar con@la que he soñado!
Ui: Bueno, yo me tengo que ir por acá. ¡Nos vemos!
Tsuruno: ¡Yo también! ¡Nos vemos después de clases!
Iroha: Ustedes dos están en mi clase, así que vayamos@juntas.
Chisato: ¡Muy bien! ¡No puedo esperar para la vida de@preparatoria~!
Iroha: Er... um... de hecho, somos de secundaria...
Chisato: [chara:406101:effect_emotion_surprise_0][se:7226_shock]¡¿Ehh?! ¡¿Quieres decir que no estás en@preparatoria?!
Chisato: ... ¡¿Así que supongo que eso significa que no@podemos tener la divertida y libre vida de@preparatoria?!
Kurumi: "La secundaria fue la única en la@que las pude meter convenientemente.@Y oye, tienen más o menos esa edad mentalmente,@tienen a Iroha ahí para hacer de niñera."
Chisato: Pues bueno. Supongo que no hay otra forma...
Takina: ... Eso me recuerda, ¿puedes ver esto,@Kurumi?
Kurumi: "Sí, gracias a esos pequeños@comunicadores que llevan puestos,@podemos hablar desde donde estamos.@Y como puedo ver el video desde aquí,@debería poder seguirlas un rato."
Kurumi: "Pero como ustedes no me necesitan aquí realmente,@estaré hablando con Touka y Nemu@mientras averiguamos como traerlas de regreso."
Chisato: ¡Gracias, Kurumi!@¿Y cómo están los demás?
Kurumi: "¿Mm? Bueno, No sé si es porque@no pueden hacer café,@o si es que ustedes dos no están aquí,@pero todos están un poco aletargados.@Bueno, Mizuki está como siempre."
Chisato: ¿Ehh? ¿Qué? Nos echan de menos...
Kurumi: "Por consecuencia, los clientes están tristes ya@la Cafetería LycoReco no puede estar abierto al público.@Esa dibujante de manga se está lamentando de que@no puede avanzar en su manuscrito."
Kurumi: "Mientras tanto, tratar de explicarle este asunto@de isekai a DA es demasiado complicado,@así que Mika ha estado tratando de interferir.@Pero por el bien de todos, será mejor@que las traigamos de regreso lo más pronto posible."
Chisato: Lo seeeeé...
Iroha: ... ¿Quién es Mika?
Chisato: El gerente de la Cafetería LycoReco.@Y quizá podría decirse que nuestro guardián.
Iroha: ¿Entonces ella es lo que Yachiyo es para Mikazuki...?
Chisato: ... Oh cielos, creo que Yachiyo te regañaría@a lo grande si hicieras esa comparación.
Chisato: El nombre suele confundir a la gente,
Chisato: pero Mika es un hombre.
Iroha: ¿Eh? Ah... ¡Pensé que era una mujer!
Chisato: *Risilla* Lo adiviné, ¿verdad?
Takina: Será mejor que nos vayamos por ahora.@Si te parece bien, Iroha.
Iroha: ¡Ah, sí! Señorita Chisato, señorita Takina, ¡Espero a@poder trabajar con ustedes!
Chisato: ¡No no no! No hay necesidad de ser formales.@¡Somos compañeras de clase, después de todo!
Iroha: Sí... No, digo...@[chara:100101:lipSynch_0][wait:0.8][chara:100101:cheek_0][chara:100101:face_mtn_ex_011.exp3.json][chara:100101:lipSynch_1]¡Sí!
Chisato: ¡Perfecto! Muy bien entonces, ¡vamos!@¡Que comience la operación encubierta!