Sana: ―Sana―@(Nuestro juego de portavasos.)
Sana: ―Sana―@(Todas fuimos de compras@para elegir este regalo para Yachiyo.)
Sana: ―Sana―@(Pensamos que podría ser un@simbolo de nuestro equipo.)
Sana: (Pero quizas yo solo lo quería para poder@sentirme parte del grupo...)
Sana: (Quizas queria mas evidencia de que hay un@lugar al que pertenezco...)
Sana: (Pero...Arruine todo...)
Sana: (Si pudiera haber sido más como Iroha y@Yachiyo...)
Sana: (Si hubiera podido mantenerme fiel a mi@misma, entonces quizás Tsuruno...)
Tsuruno: Hey, no te preocupes tanto. Si quieres@algo, solo dilo.
Sana: P-pero...
Tsuruno: ¡Somos como una familia, sabes!
Tsuruno: [chara:100301:effect_emotion_joy_0][se:7222_happy]¡Yep, yep!
Yachiyo: Bueno, yo soy la que está a cargo de las finanzas.
Yachiyo: No digas cosas así tan a la ligera.
Tsuruno: No te preocupes.
Tsuruno: Sana solo estaba diciendo que queria@pez cola amarilla a fuego lento para cenar.
Yachiyo: Oh, ¿eso es todo?
Sana: Si, pero las otras podrían no querer eso...
Yachiyo: No te preocupes por eso.
Yachiyo: Últimamente lo único que comemos es carne@ porque Felicia es egoísta.
Tsuruno: Vez, puedes ser honesta con lo que quieres.@Yachiyo te escuchara.
Sana: Está bien, muchisimas gracias.
Sana: (Tsuruno...)
Sana: (No aguanto más...)
Sana: (Quiero pelear...)
Sana: (Así que van a tener que escuchar mi@deseo egoísta...)
Felicia: Estoy tan enojada.
Felicia: No puedo quedarme quieta.
Felicia: ¿Por qué tengo que descansar?
Felicia: Nngh...
Felicia: [chara:100501:effect_emotion_sad_0][se:7224_fall_into]Gah...
Felicia: El sticker de Decagon Ball... es Aprendiz@Myoto.
Tsuruno: Whoa, ¡tengo uno! ¡Un sticker del Maestro Kushinsai!
Felicia: ¡¿Cómo?! ¡Eres tan suertuda!
Felicia: Este es mi tercer Myoto...
Tsuruno: Oh, caray.
Tsuruno: ¡En ese caso, Tsuruno hará un canje contigo!
Felicia: [chara:100501:effect_emotion_surprise_0][se:7226_shock]¡¿En serio?!
Tsuruno: Seguro. Probablemente conseguiré otro de todas formas.
Tsuruno: ¡Además! ¡Es importante para una ser amable con@su aprendiz!
Felicia: ¿Huh?
Felicia: ¡¿Cuando me volví tu aprendiz?!
Tsuruno: Si vas a hablar así, se terminó el trato.
Felicia: ¡¿Qué?!
Tsuruno: ¡Solo bromeaba!
Tsuruno: Aquí tienes.
Felicia: [chara:100501:effect_emotion_joy_0][se:7222_happy]¡Queee! ¡Eres la mejor, Tsuruno!
Felicia: ¡Tsuruno... Tsuruno... Tsuruno!
Felicia: [bgEffect:shakeSmall]¡Gaaaaaaaaaaaah!
Sana: ¡Eep! ¡¿F-Felicia?!
Felicia: [bgEffect:shakeSmall]¡Termine de esperar! ¡Solo iré!
Felicia: ¡Vamos, Sana!
Sana: ¡Pero ni siquiera sabemos a dónde ir!
Felicia: ¡Me estoy yendo!
Sana: ¡E-espera! Oh... ¡Yachiyo está llamando!
¡Bofetada!
Kaede: [flashEffect:flashRed1][se:3001_chara_damage][bgEffect:shakeSmall][chara:101101:effect_shake]¡Ahhh!
Kaede: ¡¿Qu-Qué estás haciendo, Rena?! ¡Eso dolió!
Rena: ¡No! ¡¿Qué estás haciendo TU, Kaede?!
Rena: ¡Momoko y yo nos preocupamos tanto por ti@que hasta te compramos una tarta de frutas!
Rena: ¡Creímos que te iba a hacer sentir mejor!
Rena: Cuantas más idas y vueltas y idas y vueltas@y idas y vueltas...
Rena: …¡¿va a tomar esto?!
Kaede: ¡P-pero! ¡Es porque...!
Rena: ¡No más peros!
Rena: ¡Tuve suficiente!
Rena: ¡ODIO cuando eres así, Kaede!
Rena: ¡Como eres tímida, a menos que sea algo@sobre ti misma!
Rena: ¡Como inmediatamente le ruegas a otros por ayuda!
Rena: ¡Como eres toda "oh no" y "phew" como una@beba malcriada!
Rena: ¡Como no sabes nada sobre estrellas de pop!
Rena: ¡Como tienes reptiles de mascota!
Rena: ¡Y por cierto, yo SI que se lo que es el arroz cinco-granos!
Rena: ¡Termine, ya [textRed:no somos amigas]!
¡Bofetada!
Rena: [flashEffect:flashRed1][se:3001_chara_damage][bgEffect:shakeSmall][chara:100901:effect_shake]¡Ow!
Kaede: Esa segunda parte no tiene nada que ver con@nada. ¡Y lo dijiste otra vez!
Rena: ¡Hey!
Kaede: ¡Ughhh, no me pellizques!
Rena: [bgEffect:shakeSmall][chara:100901:effect_shake]¡Grrr, puedes devolverme el pellizco, pero no duele@para nada!
Kaede: ¡¡¡...!!!
Rena: ¡¡¡...!!!
Rena: Pfft, bien...
Rena: Si tienes tanta energía, entonces no hay@de qué preocuparse.
Kaede: ¡¿...?!
Kaede: *Snif* Rena.
Kaede: Renaaaaaa... 
Kaede: *Llanto* Lo siento.
