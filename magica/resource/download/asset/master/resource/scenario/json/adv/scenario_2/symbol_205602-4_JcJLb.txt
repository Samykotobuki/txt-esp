Sayaka: No reconocí a la Mami que encontré.
Sayaka: Y lo que es más...
Sayaka: (Nos convertimos en Brujas...)
Sayaka: Pensé en lo que esas Chicas Mágicas@de Kamihama me dijeron, una y otra vez...
Sayaka: Pero simplemente no podía entenderlo.
Sayaka: (¿Qué hago ahora? Madoka, Homura...)
Sayaka: (No le encuentro ningún sentido@a lo que me dijeron.)
Sayaka: Pero ahora mismo... Ahora mismo tengo que@volver con Madoka y Homura...
Sayaka: (Pero...)
Sayaka: (Mi corazón se siente tan pesado.)
Sayaka: (Esa no era la Mami que conozco...)
Mami: Esto es lo que soy ahora...
Mami: Sayaka, aléjate de ellas...
Mami: Una vez que las elimine, escucharé lo que@tengas que decir.
Sayaka: (¿Cómo pudo decir cosas así?)
Sayaka: (¿Cómo se supone que voy a explicar@todo esto?)
Sayaka: (Sólo la idea de que las Chicas Mágicas@se conviertan en Brujas es tan...)
Sayaka: Mi cabeza estaba inundada con@todas estas nuevas revelaciones...
Sayaka: Pero no podía entender nada de eso.
Sayaka: En cualquier caso...@Tengo que volver con las demás.
