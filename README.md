# txt-esp
Este repo es solo para archivos de texto ya traducidos, ya crearemos el repo para json cuando tengamos suficientes

# Nombres y formato
Para evitar confusiones, los archivos que se suban deben llevar el mismo nombre que el archivo original correspondiente.<br/>
Ejemplo:<br/>
"symbol_101203-2.txt"       ✔️<br/>
"symbol_101203-2-esp.txt"   ❌<br/>
"esp_symbol_101203-2.txt"   ❌

# Colaboraciones
Si llegas te a este repo por casualidad o porque buscabas traducciones del juego y te gustaría ayudar a traducir Magia Record al español, puedes unirte a nuestro [Discord](https://discord.gg/SffrWJme). Aún si no te interesa traducir, es un buen lugar para discutir todo sobra la franquicia o solo platicar de otras cosas.
# Concensos de traducción
Este proyecto tiene la meta de traducir Magia Record de manera uniforme y entendible para toda la comunidad hispana. Tenemos una lista de términos, nombres, etc. que seguimos al traducir. Puedes ver la lista completa [aquí](https://gitlab.com/villa-mikazuki/txt-esp/-/wikis/home). 
